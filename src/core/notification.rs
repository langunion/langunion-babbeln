use crate::core::sound;
use egui_notify::{Toast, Toasts};
use std::ptr;
use std::sync::Once;
use std::time::Duration;

static INIT: Once = Once::new();
static mut TOASTS: *mut Toasts = ptr::null_mut();

pub fn use_toasts() -> &'static mut Toasts {
    unsafe {
        INIT.call_once(|| {
            TOASTS = Box::into_raw(Box::new(Toasts::new()));
        });
        &mut *TOASTS
    }
}

#[allow(dead_code)]
pub enum NotificationKind {
    Info,
    Success,
    Warning,
    Error,
}

pub fn create_toast(kind: NotificationKind, content: impl Into<String>) {
    let content: String = content.into();
    let toasts: &mut Toasts = use_toasts();
    let toast: &mut Toast = match kind {
        NotificationKind::Info => toasts.info(content),
        NotificationKind::Success => toasts.success(content),
        NotificationKind::Warning => toasts.warning(content),
        NotificationKind::Error => toasts.error(content),
    };
    toast.duration(Some(Duration::from_secs(10)));
}

#[cfg(not(target_arch = "wasm32"))]
pub fn create_notification(kind: NotificationKind, content: impl Into<String>) {
    use notify_rust::Notification;
    let content: String = content.into();
    if let Err(_) = Notification::new()
        .summary("babbeln")
        .body(&content)
        .icon("babbeln")
        .show()
    {
        create_toast(kind, content);
    }
    play_notification_sound();
}

#[cfg(target_arch = "wasm32")]
pub fn create_notification(kind: NotificationKind, content: impl Into<String>) {
    use crate::core::task::{spawn, wait_for};
    use wasm_bindgen_futures::JsFuture;
    use web_sys::{wasm_bindgen::JsValue, Notification, NotificationOptions};

    let content: String = content.into();

    fn desktop_notification(content: impl Into<String>) {
        let content: String = content.into();
        let options = NotificationOptions::new();
        options.set_body(&content);
        options.set_icon("babbeln");
        options.set_require_interaction(false);
        options.set_silent(Some(false));

        let notification = Notification::new_with_options("babbeln", &options).unwrap();

        spawn(async move {
            wait_for(Duration::from_secs(10)).await;
            notification.close();
        });
    }

    spawn(async move {
        let permission: String = {
            match Notification::request_permission() {
                Ok(promise) => JsFuture::from(promise)
                    .await
                    .and_then(|value: JsValue| Ok(value.as_string().unwrap_or(String::new())))
                    .ok(),
                Err(_) => None,
            }
        }
        .unwrap_or(String::new());

        match permission.as_str() {
            "granted" => {
                desktop_notification(content);
            }
            "denied" | _ => {
                create_toast(kind, content);
            }
        }
    });

    play_notification_sound();
}

pub fn play_notification_sound() {
    let bytes = include_bytes!("../../assets/sounds/notification_sound.wav");
    sound::use_sound().play_bytes(bytes, Duration::from_secs(1));
}
