use crate::core::task::spawn;
use log::debug;
use rodio::buffer::SamplesBuffer;
use rodio::{Decoder, OutputStream, OutputStreamHandle, Sink};
use std::collections::HashMap;
use std::io::Cursor;
use std::ptr;
use std::sync::Once;
use std::time::Duration;

static INIT: Once = Once::new();
static mut SOUND_CONTEXT: *mut SoundContext = ptr::null_mut();

pub fn use_sound() -> &'static mut SoundContext {
    unsafe {
        INIT.call_once(|| {
            debug!("Created SOUND_HANDLE");
            SOUND_CONTEXT = Box::into_raw(Box::new(SoundContext::new()));
        });
        &mut *SOUND_CONTEXT
    }
}

pub struct SoundContext {
    _stream: OutputStream,
    stream_handle: OutputStreamHandle,
    sinks: HashMap<&'static str, Sink>,
}

impl SoundContext {
    pub fn new() -> Self {
        let (stream, stream_handle) = OutputStream::try_default().unwrap();
        Self {
            _stream: stream,
            stream_handle,
            sinks: HashMap::new(),
        }
    }

    pub fn play(&mut self, sink: &'static str, sample: SamplesBuffer<f32>) {
        let sink = match self.sinks.contains_key(sink) {
            true => self.sinks.get_mut(sink).unwrap(),
            false => {
                self.sinks
                    .insert(sink, Sink::try_new(&self.stream_handle).unwrap());
                self.sinks.get_mut(sink).unwrap()
            }
        };
        sink.append(sample);
        sink.play();
    }

    pub fn play_bytes(&self, bytes: &'static [u8], _duration: Duration) {
        let sink = Sink::try_new(&self.stream_handle).unwrap();
        spawn(async move {
            let bytes = Cursor::new(bytes);
            let source = Decoder::new(bytes).unwrap();
            sink.append(source);

            #[cfg(target_arch = "wasm32")]
            {
                use crate::core::task::wait_for;
                sink.play();
                wait_for(_duration).await;
            }

            #[cfg(not(target_arch = "wasm32"))]
            sink.sleep_until_end();
        });
    }
}
