use image::{ImageBuffer, Rgba};
use wasm_bindgen_futures::JsFuture;
use web_sys::wasm_bindgen::JsCast;
use web_sys::{HtmlVideoElement, MediaStream, OffscreenCanvas, OffscreenCanvasRenderingContext2d};

pub struct VideoEncoder {}

impl VideoEncoder {
    pub fn new() -> Self {
        todo!()
    }

    pub fn encode_to_vp8() -> Self {
        todo!()
    }
}

pub struct VideoDecoder {
    video: HtmlVideoElement,
    canvas: OffscreenCanvas,
    context: OffscreenCanvasRenderingContext2d,
    width: u32,
    height: u32,
}

impl VideoDecoder {
    pub async fn new(stream: &MediaStream) -> Self {
        let video = web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .create_element("video")
            .unwrap()
            .dyn_into::<HtmlVideoElement>()
            .unwrap();
        video.set_src_object(Some(stream));
        video.set_muted(true);
        JsFuture::from(video.play().expect("Could not start video"))
            .await
            .expect("Could not await video play future");

        let video_width = video.video_width();
        let video_height = video.video_height();

        assert_ne!(video_width, 0, "Video width should not be 0");
        assert_ne!(video_height, 0, "Video height should not be 0");

        let canvas = OffscreenCanvas::new(video_width, video_height)
            .expect("Could not create offset canavas element");
        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<OffscreenCanvasRenderingContext2d>()
            .unwrap();
        let canvas_width = canvas.width();
        let canvas_height = canvas.height();

        assert_ne!(canvas_width, 0, "Canvas width should not be 0");
        assert_ne!(canvas_height, 0, "Canvas height should not be 0");

        assert_eq!(
            video_width, canvas_width,
            "Video width '{video_width}px' and canvas width '{canvas_width}px' do not match"
        );
        assert_eq!(
            video_height, canvas_height,
            "Video height '{video_height}px' and canvas height '{canvas_height}px' do not match"
        );

        Self {
            video,
            canvas,
            context,
            width: video_width,
            height: video_height,
        }
    }

    pub fn decode_from_vp8(&self) -> Option<ImageBuffer<Rgba<u8>, Vec<u8>>> {
        self.context
            .draw_image_with_html_video_element(&self.video, 0.0, 0.0)
            .unwrap();
        let image_data = self
            .context
            .get_image_data(0.0, 0.0, self.width as f64, self.height as f64)
            .unwrap();
        let rgba_data = {
            let pixels: Vec<u8> = image_data.data().to_vec();
            let mut rgba_data = Vec::new();
            for chunk in pixels.chunks(4) {
                let r = chunk[0];
                let g = chunk[1];
                let b = chunk[2];
                let a = 255;
                rgba_data.push(r);
                rgba_data.push(g);
                rgba_data.push(b);
                rgba_data.push(a);
            }
            rgba_data
        };
        ImageBuffer::from_vec(self.width, self.height, rgba_data)
    }
}

pub struct AudioEncoder {}

impl AudioEncoder {
    pub fn new() -> Self {
        todo!()
    }

    pub fn encode_to_opus(&mut self) -> Vec<u8> {
        todo!()
    }
}

pub struct AudioDecoder {}

impl AudioDecoder {
    pub fn new() -> Self {
        todo!()
    }

    pub fn decode_from_opus(&mut self) -> Vec<f32> {
        todo!()
    }
}
