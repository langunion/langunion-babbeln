use crate::core::transcoder::AudioEncoder;
use bytes::Bytes;
use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    InputCallbackInfo, InputStreamTimestamp, SampleFormat, SampleRate, Stream, SupportedBufferSize,
    SupportedStreamConfig,
};
use log::{debug, error};
use std::{sync::Arc, time::Duration};
use webrtc::{
    media::Sample, track::track_local::track_local_static_sample::TrackLocalStaticSample,
};

struct SendStream(Stream);
unsafe impl Send for SendStream {}

pub struct Microphone {
    stream: SendStream,
}

impl Microphone {
    pub fn new(audio_track: Arc<TrackLocalStaticSample>) -> Self {
        let host = cpal::default_host();
        let device = host
            .default_input_device()
            .expect("No default microphone output found");
        let channels = 1;
        let sample_rate = 48_000;
        let config = SupportedStreamConfig::new(
            channels,
            SampleRate(sample_rate),
            SupportedBufferSize::Unknown,
            SampleFormat::F32,
        )
        .config();

        let mut samples_buffer: Vec<f32> = Vec::new();
        let mut opus_encoder = AudioEncoder::new();

        let stream = device
            .build_input_stream(
                &config,
                move |data: &[f32], info: &InputCallbackInfo| {
                    let InputStreamTimestamp { callback, capture } = info.timestamp();
                    let duration = callback
                        .duration_since(&capture)
                        .unwrap_or(Duration::default());
                    samples_buffer.extend_from_slice(data);

                    while samples_buffer.len() > 960 {
                        let frame: Vec<f32> = samples_buffer.drain(..960).collect();
                        let opus_data = opus_encoder.encode_to_opus(&frame);
                        let webrtc_sample = Sample {
                            data: Bytes::from(opus_data),
                            duration,
                            ..Default::default()
                        };
                        let audio_track = audio_track.clone();
                        futures::executor::block_on(async move {
                            let _ = audio_track.write_sample(&webrtc_sample).await;
                        });
                    }
                },
                |err| error!("{err}"),
                None,
            )
            .expect("Could not build microphone stream");
        stream.pause().expect("Could not pause stream");

        Self {
            stream: SendStream(stream),
        }
    }

    pub fn start_recording(&self) {
        debug!("Started recording microphone");
        self.stream.0.play().expect("Could not start recording");
    }

    pub fn pause_recording(&self) {
        debug!("Paused recording microphone");
        self.stream.0.pause().expect("Could not pause stream");
    }

    pub fn stop_recording(self) {
        debug!("Stopped recording microphone");
        self.stream.0.pause().expect("Could not stop stream");
    }
}
