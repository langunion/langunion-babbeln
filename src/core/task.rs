use std::future::Future;
use std::time::Duration;
use tokio::sync::oneshot;

#[cfg(not(target_arch = "wasm32"))]
pub fn spawn<F>(async_closure: F)
where
    F: Future<Output = ()> + Send + 'static,
{
    tokio::spawn(async_closure);
}

#[cfg(target_arch = "wasm32")]
pub fn spawn<F>(async_closure: F)
where
    F: Future<Output = ()> + 'static,
{
    wasm_bindgen_futures::spawn_local(async_closure);
}

#[cfg(not(target_arch = "wasm32"))]
pub fn spawn_blocking<F>(f: F)
where
    F: FnOnce() + Send + 'static,
{
    tokio::task::spawn_blocking(f);
}

#[cfg(target_arch = "wasm32")]
pub fn spawn_blocking<F>(f: F)
where
    F: FnOnce() + Send + 'static,
{
    // TODO wasm doesnt support threading yet and web workers are to
    // difficult for me to integrate so for now i dont care for
    // web perfomance which makes me very sad ;(
    spawn(async move {
        f();
    });

    // let worker = web_sys::Worker::new("./worker.js").unwrap();
    // let ptr = Box::into_raw(Box::new(Box::new(f) as Box<dyn FnOnce()>));
    // let msg = js_sys::Array::new();
    // msg.push(&wasm_bindgen::memory());
    // msg.push(&wasm_bindgen::JsValue::from(ptr as u32));
    // let _ = worker.post_message(&msg);
}

// #[cfg(target_arch = "wasm32")]
// #[wasm_bindgen::prelude::wasm_bindgen]
// pub fn worker_entry_point(ptr: u32) {
//   let closure = unsafe { Box::from_raw(ptr as *mut Box<dyn FnOnce()>) };
//   (*closure)();
// }

/// Simple helper function to handle a oneshot receiver
/// inside the eframe render loop
pub fn handle_oneshot_receiver<T>(
    maybe_receiver: &mut Option<oneshot::Receiver<T>>,
    on_success: impl FnOnce(T),
) {
    if let Some(receiver) = maybe_receiver {
        match receiver.try_recv() {
            Ok(t) => on_success(t),
            Err(oneshot::error::TryRecvError::Closed) => *maybe_receiver = None,
            Err(oneshot::error::TryRecvError::Empty) => (),
        }
    }
}

/// Wait/sleep inside the current thread
/// for a passed in duration
#[cfg(not(target_arch = "wasm32"))]
pub async fn wait_for(duration: Duration) {
    tokio::time::sleep(duration).await;
}

/// Same as the rust version of wait_for.
/// Creates web_sys/Rust version of this:
/// new Promise((resolve) => setTimeout(() => resolve(null), duration);
#[cfg(target_arch = "wasm32")]
pub async fn wait_for(duration: Duration) {
    let duration: u32 = duration.as_millis() as u32;
    gloo::timers::future::TimeoutFuture::new(duration).await;
}
