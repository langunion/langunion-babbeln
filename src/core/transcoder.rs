#![allow(unused_imports)]
#[cfg(not(target_arch = "wasm32"))]
pub use super::transcoder_native::{AudioDecoder, AudioEncoder, VideoDecoder, VideoEncoder};
#[cfg(target_arch = "wasm32")]
pub use super::transcoder_wasm::{AudioDecoder, AudioEncoder, VideoDecoder, VideoEncoder};
