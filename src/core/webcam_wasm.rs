use crate::actors::use_webrtc_handle;
use crate::core::task::{spawn, wait_for};
use crate::core::transcoder::VideoDecoder;
use std::time::Duration;
use tokio::sync::oneshot;
use wasm_bindgen_futures::JsFuture;
use web_sys::{wasm_bindgen::JsCast, MediaStream, MediaStreamConstraints};

pub struct WebcamDevice {
    id: String,
}

pub struct WebcamHandle {
    stop_recording_sender: oneshot::Sender<()>,
}

impl WebcamHandle {
    pub fn stop_recording(self) {
        let _ = self.stop_recording_sender.send(());
    }
}

pub struct Webcam {
    decoder: VideoDecoder,
    stop_recording_receiver: oneshot::Receiver<()>,
}

impl Webcam {
    pub async fn new() -> (Self, WebcamHandle, MediaStream) {
        let media_stream: MediaStream = {
            let window = web_sys::window().unwrap();
            let navigator = window.navigator();
            let media_devices = navigator.media_devices().unwrap();
            let options = MediaStreamConstraints::new();
            options.set_video(&true.into());
            options.set_audio(&false.into());
            let stream = media_devices
                .get_user_media_with_constraints(&options)
                .unwrap();
            let stream = JsFuture::from(stream)
                .await
                .unwrap()
                .dyn_into::<MediaStream>()
                .unwrap();
            stream
        };

        let decoder = VideoDecoder::new(&media_stream).await;
        let (sender, receiver) = oneshot::channel::<()>();
        let webcam_handle = WebcamHandle {
            stop_recording_sender: sender,
        };

        (
            Self {
                decoder,
                stop_recording_receiver: receiver,
            },
            webcam_handle,
            media_stream,
        )
    }

    pub fn start_recording(mut self) {
        let webrtc_handle = use_webrtc_handle();
        spawn(async move {
            loop {
                if let Ok(_) = self.stop_recording_receiver.try_recv() {
                    break;
                }
                if let Some(image) = self.decoder.decode_from_vp8() {
                    let _ = webrtc_handle.media_stream_channel(0).video.0.send(image);
                }
                let one_frame_in_sec = 1 / 60;
                wait_for(Duration::from_secs_f32(one_frame_in_sec as f32)).await;
            }
        });
    }

    pub fn devices(&mut self) -> Vec<WebcamDevice> {
        todo!()
    }
}
