use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct WebRtcEvent {
    pub action: WebRtcAction,
    pub payload: String,
    pub sender_user_id: usize,
    pub sender_socket_id: Option<String>,
    pub receiver_user_id: usize,
    pub receiver_socket_id: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum WebRtcAction {
    SignalingOffer,
    SignalingAnswer,
    Ice,
    RenegotiateOffer,
    RenegotiateAnswer,
}
