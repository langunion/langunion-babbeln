use bytes::Bytes;
use ffmpeg::software::scaling::{context::Context as Scaler, flag::Flags};
use ffmpeg::{codec, decoder, format, util};
use ffmpeg_next as ffmpeg;
use image::{ImageBuffer, Rgba};

pub type TranscoderError = ffmpeg::Error;

pub struct VideoEncoder {
    encoder: ffmpeg::encoder::Video,
    frame: ffmpeg::frame::Video,
}

impl VideoEncoder {
    pub fn new() -> Self {
        ffmpeg::init().unwrap();
        let encoder = {
            let codec = ffmpeg::codec::encoder::find(ffmpeg::codec::Id::VP8).unwrap();
            let mut context = ffmpeg::codec::Context::new_with_codec(codec);
            context.set_time_base((1, 60));
            // context.set_threading(config)
            let mut encoder = context.encoder().video().unwrap();
            encoder.set_width(500);
            encoder.set_height(500);
            encoder.set_format(ffmpeg::format::Pixel::YUV420P);
            // encoder.set_bit_rate(value);
            // encoder.set_max_bit_rate(value);
            // encoder.set_frame_rate(value);
            encoder.open_as(codec).unwrap()
        };
        let mut frame = ffmpeg::frame::Video::empty();
        frame.set_format(ffmpeg::format::Pixel::RGBA);

        Self { encoder, frame }
    }

    pub fn encode_to_vp8(
        &mut self,
        input: ImageBuffer<Rgba<u8>, Vec<u8>>,
    ) -> Result<Vec<u8>, TranscoderError> {
        // Copy RGBA data to frame
        self.frame.data_mut(0).copy_from_slice(&input.to_vec());
        // Create temporary frame for YUV conversion
        let mut yuv_frame = ffmpeg::frame::Video::new(
            ffmpeg::format::Pixel::YUV420P,
            self.frame.width(),
            self.frame.height(),
        );
        // yuv_frame.set_pts(Some(self.pts));
        // Convert RGBA to YUV420P
        let mut scaler = ffmpeg::software::scaling::Context::get(
            ffmpeg::format::Pixel::RGBA,
            self.frame.width(),
            self.frame.height(),
            ffmpeg::format::Pixel::YUV420P,
            self.frame.width(),
            self.frame.height(),
            ffmpeg::software::scaling::Flags::BILINEAR,
        )?;
        scaler.run(&self.frame, &mut yuv_frame)?;
        // Encode the frame
        let mut output = Vec::new();
        self.encoder.send_frame(&yuv_frame)?;
        let mut packet = ffmpeg::Packet::empty();
        while self.encoder.receive_packet(&mut packet).is_ok() {
            output.extend_from_slice(packet.data().unwrap());
        }
        Ok(output)
    }
}

pub struct VideoDecoder {
    decoder: ffmpeg::decoder::Video,
}

impl VideoDecoder {
    pub fn new() -> Self {
        ffmpeg::init().unwrap();
        let decoder = {
            let codec = decoder::find(codec::Id::VP8).expect("Could not find codec");
            let context = codec::Context::new_with_codec(codec);
            context.decoder().video().unwrap()
        };

        Self { decoder }
    }

    pub fn decode_from_vp8(&mut self, input: Bytes) -> Result<Vec<u8>, TranscoderError> {
        let mut input_frame = util::frame::Video::empty();
        let mut output_frame = util::frame::Video::empty();
        let packet = ffmpeg::Packet::copy(&input);
        self.decoder.send_packet(&packet)?;
        self.decoder.receive_frame(&mut input_frame)?;
        let width = input_frame.width();
        let height = input_frame.height();
        let mut scaler = Scaler::get(
            input_frame.format(),
            width,
            height,
            format::Pixel::RGBA,
            width,
            height,
            Flags::BILINEAR,
        )?;
        scaler.run(&input_frame, &mut output_frame)?;
        Ok(output_frame.data(0).to_vec())
    }
}

pub struct AudioEncoder {
    encoder: ffmpeg::encoder::Audio,
    frame: ffmpeg::frame::Audio,
}

impl AudioEncoder {
    pub fn new() -> Self {
        ffmpeg::init().unwrap();
        let encoder = {
            let codec = ffmpeg::codec::encoder::find(ffmpeg::codec::Id::OPUS).unwrap();
            let context = ffmpeg::codec::Context::new_with_codec(codec);
            let mut encoder = context.encoder().audio().unwrap();
            encoder.set_bit_rate(128_000); // 128 kbps
            encoder.set_rate(48_000); // 48 kHz
            encoder.set_channel_layout(ffmpeg::ChannelLayout::MONO);
            encoder.set_format(ffmpeg::format::Sample::F32(
                ffmpeg::format::sample::Type::Packed,
            ));
            encoder.set_time_base((1, 48_000));
            encoder.open_as(codec).unwrap()
        };

        let mut frame = ffmpeg::frame::Audio::new(
            ffmpeg::format::Sample::F32(ffmpeg::format::sample::Type::Packed),
            960,
            ffmpeg::ChannelLayout::MONO,
        );
        frame.set_rate(48_000);
        frame.set_channel_layout(ffmpeg::ChannelLayout::MONO);

        Self { encoder, frame }
    }

    pub fn encode_to_opus(&mut self, input: &[f32]) -> Vec<u8> {
        assert_eq!(
            input.len(),
            960,
            "Input sample size must be 960 samples (20ms at 48kHz) it was {}",
            input.len()
        );
        let frame_data = self.frame.plane_mut(0);
        frame_data[..input.len()].copy_from_slice(input);
        self.encoder.send_frame(&self.frame).unwrap();
        let mut output: Vec<u8> = Vec::new();
        let mut packet = ffmpeg_next::Packet::empty();
        while let Ok(_) = self.encoder.receive_packet(&mut packet) {
            output.extend_from_slice(packet.data().unwrap());
        }
        output
    }
}

pub struct AudioDecoder {}

impl AudioDecoder {
    pub fn new() -> Self {
        ffmpeg::init().unwrap();
        Self {}
    }

    pub fn decode_from_opus(&mut self) -> Vec<f32> {
        todo!()
    }
}
