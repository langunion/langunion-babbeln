#[cfg(not(target_arch = "wasm32"))]
pub use super::webcam_native::*;
#[cfg(target_arch = "wasm32")]
pub use super::webcam_wasm::*;
