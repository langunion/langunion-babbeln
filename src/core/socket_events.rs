use log::warn;
use serde::{de::DeserializeOwned, Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct SocketEvent {
    pub action: SocketAction,
    pub payload: String,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum SocketAction {
    // Users
    DeleteUser,
    UpdateUser,
    UpdateUserAvatar,
    UserWentOnline,
    UserWentOffline,
    // Users - Friends
    RemoveFriend,
    // Users - BlockedUsers
    BlockUser,
    UnblockUser,
    // Tokens
    ResetTokens,
    // Friendrequests
    SendFriendrequest,
    AcceptFriendrequest,
    DeclineFriendrequest,
    // Posts
    CreatePost,
    RemovePost,
    LikePost,
    UnlikePost,
    CommentPost,
    RemoveCommentPost,
    // Message
    SendMessage,
    UpdateMessage,
    DeleteMessage,
    DeleteMessagePrivately,
    DeleteChat,
}

impl SocketEvent {
    pub fn parse_payload<T>(&self) -> Option<T>
    where
        T: DeserializeOwned,
    {
        serde_json::from_str(&self.payload)
            .map_err(|e| {
                warn!("{e}");
                warn!("Got socket event payload which doesn't match local socket event type");
                e
            })
            .ok()
    }
}
