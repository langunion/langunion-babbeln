use crate::actors::use_webrtc_handle;
use crate::core::transcoder::VideoEncoder;
use bytes::Bytes;
use log::{debug, error};
use nokhwa::pixel_format::RgbAFormat;
use nokhwa::utils::{CameraIndex, RequestedFormat, RequestedFormatType};
use nokhwa::Camera;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::oneshot;
use webrtc::media::Sample;
use webrtc::track::track_local::track_local_static_sample::TrackLocalStaticSample;

pub struct WebcamDevice {
    id: String,
}

pub struct WebcamHandle {
    stop_recording_sender: oneshot::Sender<()>,
}

impl WebcamHandle {
    pub fn stop_recording(self) {
        let _ = self.stop_recording_sender.send(());
        debug!("Stopped recording webcam");
    }
}

pub struct Webcam {
    camera: Camera,
    stop_recording_receiver: oneshot::Receiver<()>,
}

impl Webcam {
    pub fn new() -> (Self, WebcamHandle) {
        let index = CameraIndex::Index(0);
        let requested =
            RequestedFormat::new::<RgbAFormat>(RequestedFormatType::AbsoluteHighestFrameRate);
        let camera = Camera::new(index, requested).unwrap();
        let (sender, receiver) = oneshot::channel::<()>();
        (
            Self {
                camera,
                stop_recording_receiver: receiver,
            },
            WebcamHandle {
                stop_recording_sender: sender,
            },
        )
    }

    pub fn start_recording(mut self, video_track: Arc<TrackLocalStaticSample>) {
        self.camera.open_stream().unwrap();
        let webrtc_handle = use_webrtc_handle();
        let mut encoder = VideoEncoder::new();
        std::thread::spawn(move || loop {
            if let Ok(_) = self.stop_recording_receiver.try_recv() {
                break;
            }
            let video_track = video_track.clone();
            let frame_rate = self.camera.frame_rate() as usize;
            let frame = self.camera.frame().unwrap();
            let image = frame.decode_image::<RgbAFormat>().unwrap();
            let _ = webrtc_handle
                .media_stream_channel(0)
                .video
                .0
                .send(image.clone());
            match encoder.encode_to_vp8(image) {
                Ok(vp8_image) => {
                    let sample = Self::webrtc_sample(vp8_image, frame_rate);
                    futures::executor::block_on(async move {
                        let _ = video_track.write_sample(&sample).await;
                    });
                }
                Err(e) => error!("{e}"),
            };
        });
        debug!("Started recording webcam");
    }

    pub fn devices(&mut self) -> Vec<WebcamDevice> {
        todo!()
    }

    fn webrtc_sample(vp8_img: Vec<u8>, frame_rate: usize) -> webrtc::media::Sample {
        let duration = Duration::from_secs_f32(1.0 / frame_rate as f32);
        Sample {
            data: Bytes::from(vp8_img),
            duration,
            ..Default::default()
        }
    }
}
