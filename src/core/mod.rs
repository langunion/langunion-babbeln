pub mod microphone;
pub mod models;
pub mod notification;
pub mod responses;
pub mod socket_events;
pub mod sound;
pub mod task;
pub mod transcoder;
pub mod webcam;
pub mod webrtc_events;

#[cfg(not(target_arch = "wasm32"))]
pub mod webcam_native;
#[cfg(target_arch = "wasm32")]
pub mod webcam_wasm;

#[cfg(not(target_arch = "wasm32"))]
pub mod microphone_native;
#[cfg(target_arch = "wasm32")]
pub mod microphone_wasm;

#[cfg(not(target_arch = "wasm32"))]
pub mod transcoder_native;
#[cfg(target_arch = "wasm32")]
pub mod transcoder_wasm;
