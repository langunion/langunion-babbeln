use wasm_bindgen_futures::JsFuture;
use web_sys::{wasm_bindgen::JsCast, MediaStream, MediaStreamConstraints};

pub struct Microphone {}

impl Microphone {
    pub async fn new() -> MediaStream {
        let media_stream: MediaStream = {
            let window = web_sys::window().unwrap();
            let navigator = window.navigator();
            let media_devices = navigator.media_devices().unwrap();
            let options = MediaStreamConstraints::new();
            options.set_video(&false.into());
            options.set_audio(&true.into());
            let stream = media_devices
                .get_user_media_with_constraints(&options)
                .unwrap();
            let stream = JsFuture::from(stream)
                .await
                .unwrap()
                .dyn_into::<MediaStream>()
                .unwrap();
            stream
        };
        media_stream
    }
}
