use crate::core::models::User;
use egui::{Color32, Context, Ui};
use egui_modal::{Modal, ModalStyle};
use log::debug;
use uuid::Uuid;

pub struct ProfileModal {
    id: String,
    modal: Option<Modal>,
    modal_style: ModalStyle,
    user: User,
}

impl ProfileModal {
    pub fn new(user: User) -> Self {
        let id: String = Uuid::new_v4().to_string();
        let mut modal_style = ModalStyle::default();
        modal_style.overlay_color = Color32::from_rgba_unmultiplied(0, 0, 0, 90);

        Self {
            id,
            modal: None,
            modal_style,
            user,
        }
    }

    pub fn render(&mut self, _ui: &mut Ui, ctx: &Context) {
        let modal = Modal::new(ctx, &self.id).with_style(&self.modal_style);

        modal.show(|ui: &mut Ui| {
            modal.title(ui, &self.user.username);

            modal.frame(ui, |ui: &mut Ui| {
                ui.add(egui::Image::new(&self.user.avatar).rounding(10.));
            });

            modal.buttons(ui, |ui: &mut Ui| modal.button(ui, "close"));
        });

        self.modal = Some(modal);
    }

    pub fn open(&mut self) {
        debug!("Open profile modal for {}", self.user.username);
        self.modal.as_ref().unwrap().open();
    }
}
