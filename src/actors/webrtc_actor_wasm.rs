use crate::actors::webrtc_actor::{ActorMessage, WebrtcHandle};
use crate::core::microphone::Microphone;
use crate::core::task::spawn;
use crate::core::webcam::{Webcam, WebcamHandle};
use log::{debug, error, info};
use std::sync::Arc;
use tokio::sync::{mpsc, watch};
use wasm_bindgen_futures::JsFuture;
use web_sys::js_sys::Array;
use web_sys::wasm_bindgen::{closure::Closure, JsCast};
use web_sys::{
    window, MediaStream, MediaStreamConstraints, MediaStreamTrack, MediaStreamTrackEvent,
};
use web_sys::{
    MessageEvent, RtcConfiguration, RtcDataChannelEvent, RtcIceServer, RtcPeerConnection,
    RtcPeerConnectionIceEvent, RtcSdpType, RtcSessionDescriptionInit,
};

pub struct WebrtcActor {
    peer_connection: Option<Arc<RtcPeerConnection>>,
    peer_connection_media_pipeline: Option<(WebcamHandle, ())>,
    screenshare_peer_connection: Option<Arc<RtcPeerConnection>>,
    // screenshare_peer_media_connection_pipeline: Option<ScreenshareHandle>,
    call_key: watch::Sender<Option<String>>,
    is_in_call: watch::Sender<bool>,
    is_webcam_on: watch::Sender<bool>,
    is_microphone_on: watch::Sender<bool>,
    is_screenshare_on: watch::Sender<bool>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl WebrtcActor {
    pub fn new() -> (Self, WebrtcHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_call_key, watcher_call_key) = watch::channel::<Option<String>>(None);
        let (update_is_in_call, watcher_is_in_call) = watch::channel::<bool>(false);
        let (update_is_webcam_on, watcher_is_webcam_on) = watch::channel::<bool>(false);
        let (update_is_microphone_on, watcher_is_microphone_on) = watch::channel::<bool>(false);
        let (update_is_screenshare_on, watcher_is_screenshare_on) = watch::channel::<bool>(false);

        let webrtc_actor = Self {
            peer_connection: None,
            peer_connection_media_pipeline: None,
            screenshare_peer_connection: None,
            // screenshare_peer_connection_media_pipeline: None,
            call_key: update_call_key,
            is_in_call: update_is_in_call,
            is_webcam_on: update_is_webcam_on,
            is_microphone_on: update_is_microphone_on,
            is_screenshare_on: update_is_screenshare_on,
            receiver,
        };

        let webrtc_handle = WebrtcHandle::new(
            watcher_call_key,
            watcher_is_in_call,
            watcher_is_webcam_on,
            watcher_is_microphone_on,
            watcher_is_screenshare_on,
            sender,
        );

        (webrtc_actor, webrtc_handle)
    }

    pub fn run(webrtc_actor: Self) {
        debug!("Started WebrtcActor event loop");
        spawn(webrtc_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                // Stop the local stream and media pipeline
                ActorMessage::Reset => {
                    debug!("ActorMessage::Reset");
                    // Stop the local stream
                    if let Some(peer_connection) = self.peer_connection.take() {
                        peer_connection
                            .get_local_streams()
                            .into_iter()
                            .for_each(|stream| {
                                let stream = stream.dyn_into::<MediaStream>().unwrap();
                                stream.get_tracks().into_iter().for_each(|track| {
                                    let track = track.dyn_into::<MediaStreamTrack>().unwrap();
                                    track.stop();
                                })
                            })
                    }
                    // Stop the media pipeline that renders the webcam etc.
                    if let Some(pipeline) = self.peer_connection_media_pipeline.take() {
                        pipeline.0.stop_recording();
                    }
                    self.screenshare_peer_connection = None;
                    // if let Some(pipeline) = self.screenshare_peer_connection_media_pipeline.take() {
                    //     drop(pipeline.0);
                    //     drop(pipeline.1);
                    // }
                    let _ = self.call_key.send(None);
                    let _ = self.is_in_call.send(false);
                    let _ = self.is_webcam_on.send(false);
                    let _ = self.is_microphone_on.send(false);
                    let _ = self.is_screenshare_on.send(false);
                }
                ActorMessage::StartCall => {
                    debug!("ActorMessage::StartCall");
                    let (peer_connection, pipeline) = Self::new_peer_connection().await;
                    self.peer_connection = Some(peer_connection);
                    self.peer_connection_media_pipeline = Some(pipeline);
                }
                ActorMessage::SetCallKey(call_key) => {
                    debug!("ActorMessage::SetCallKey");
                    let _ = self.call_key.send(call_key);
                }
                ActorMessage::EnableWebcam => {
                    debug!("ActorMessage::EnableWebcam");
                    let _ = self.is_webcam_on.send(true);
                }
                ActorMessage::DisableWebcam => {
                    debug!("ActorMessage::DisableWebcam");
                    let _ = self.is_webcam_on.send(false);
                }
                ActorMessage::EnableMicrophone => {
                    debug!("ActorMessage::EnableMicrophone");
                    let _ = self.is_microphone_on.send(true);
                }
                ActorMessage::DisableMicrophone => {
                    debug!("ActorMessage::DisableMicrophone");
                    let _ = self.is_microphone_on.send(false);
                }
                ActorMessage::EnableScreenshare => {
                    debug!("ActorMessage::EnableScreenshare");
                    let _ = self.is_screenshare_on.send(true);
                }
                ActorMessage::DisableSceenshare => {
                    debug!("ActorMessage::DisableScreenshare");
                    let _ = self.is_screenshare_on.send(false);
                }
            }
        }
    }

    async fn new_peer_connection() -> (Arc<RtcPeerConnection>, (WebcamHandle, ())) {
        let config = RtcConfiguration::new();
        let ice_server = RtcIceServer::new();
        ice_server.set_url("stun:stun.l.google.com:19302");
        let ice_servers = vec![ice_server];
        config.set_ice_servers(&ice_servers.into());
        let peer_connection = Arc::new(RtcPeerConnection::new_with_configuration(&config).unwrap());
        let pipeline = Self::mount_media_streams(peer_connection.clone()).await;
        (peer_connection, pipeline)

        // TODO event listeners
        // /* add icecandidate event listener
        //  * if a new icecandidate is found emit it to the remote peer */
        // newPeerConnection.addEventListener("icecandidate", (event: RTCPeerConnectionIceEvent) => {
        //   const icecandidate: RTCIceCandidate | null = event.candidate;
        //   if (socketContext.socket) {
        //     logger.log.magenta("CallContext", "Socket event emitted signaling_icecandidate");
        //     socketContext.socket.emit(
        //       "signaling_icecandidate",
        //       {
        //         fromUser: userContext.loggedInUserId, /* the id of the user emitting the event */
        //         toUser: withUser, /* the id of the user to whom the candidate should be sent */
        //         toSocketid: withSocketid, /* the socketid of the device of the remote user */
        //         candidate: icecandidate, /* candidate */
        //         key: key!, /* the security key to make the signaling process more secure */
        //       }
        //     );
        //   }
        // });

        // newPeerConnection.addEventListener("negotiationneeded", (event: Event) => {
        //   logger.log.magenta("CallContext", "peerConnection needs (re)negotiation.");
        // })

        // /* if remote peer loses connection close the call */
        // newPeerConnection.addEventListener("connectionstatechange", (_event: Event) => {
        //   logger.log.magenta("CallContext", `Connection State changed: ${newPeerConnection.connectionState}`);
        //   if (newPeerConnection.connectionState === "disconnected") return hangup("on");
        //   if (newPeerConnection.connectionState === "connected") return notifContext.playSoundStartCall();
        //   if (newPeerConnection.connectionState === "failed") return startRenegotiation();
        // });
    }

    async fn mount_media_streams(
        peer_connection: Arc<web_sys::RtcPeerConnection>,
    ) -> (WebcamHandle, ()) {
        let (webcam, webcam_handle, video_stream) = Webcam::new().await;
        let audio_stream = Microphone::new().await;

        webcam.start_recording();

        video_stream.get_tracks().into_iter().for_each(|t| {
            let t = t.dyn_into::<MediaStreamTrack>().unwrap();
            peer_connection.add_track(&t, &video_stream, &Array::new());
        });

        audio_stream.get_tracks().into_iter().for_each(|t| {
            let t = t.dyn_into::<MediaStreamTrack>().unwrap();
            peer_connection.add_track(&t, &audio_stream, &Array::new());
        });

        let ontrack_callback = Closure::<dyn FnMut(_)>::new(move |e: MediaStreamTrackEvent| {
            match e.track().kind().as_str() {
                "audio" => {
                    // TODO replace current remote audio stream
                    todo!();
                }
                "video" => {
                    // TODO replace current remote video stream
                    todo!();
                }
                _ => {}
            }
        });

        peer_connection.set_ontrack(Some(ontrack_callback.as_ref().unchecked_ref()));
        ontrack_callback.forget();
        (webcam_handle, ())
    }
}
