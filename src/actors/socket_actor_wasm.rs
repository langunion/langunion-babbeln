use crate::actors::socket_actor::HandleSocketEvents;
use crate::actors::{use_app_handle, use_socket_handle, SocketHandle};
use crate::core::task::{spawn, wait_for};
use eframe::wasm_bindgen::{closure::Closure, JsCast};
use log::{debug, error, info};
use std::time::Duration;
use tokio::sync::mpsc;
use web_sys::{js_sys, CloseEvent, ErrorEvent, MessageEvent, WebSocket};

#[derive(Debug)]
pub enum ActorMessage {
    Connect,
    Disconnect,
}

pub struct SocketActor {
    socket: Option<WebSocket>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl HandleSocketEvents for SocketActor {}

impl SocketActor {
    pub fn new() -> (Self, SocketHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();
        let socket_actor = Self {
            socket: None,
            receiver,
        };
        let socket_handle = SocketHandle::new(sender);
        (socket_actor, socket_handle)
    }

    pub fn run(socket_actor: Self) {
        debug!("Started SocketActor event loop");
        spawn(socket_actor.event_loop());
    }

    pub fn connect() -> WebSocket {
        info!("Connect to socket server");
        #[cfg(debug_assertions)]
        let uri: String = "ws://localhost:4000/v1/ws/".to_string();
        #[cfg(not(debug_assertions))]
        let uri: String = "ws://server.langunion.com/v1/ws/".to_string();
        // uses cookie as authentication method
        let socket = WebSocket::new(&uri)
            .expect("Failed to connect to websocket server due url being wrong format");
        Self::mount_event_handlers(&socket);
        socket
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Connect => {
                    debug!("ActorMessage::Connect");
                    assert!(
                        self.socket.is_none(),
                        "Can't connect to socket server if there is still a pending connection. Call 'disconnect' first",
                    );
                    if let Some(socket) = self.socket.take() {
                        Self::unmount_event_handlers(&socket);
                        let _ = socket.close();
                    }
                    let socket = Self::connect();
                    self.socket = Some(socket);
                }
                ActorMessage::Disconnect => {
                    debug!("ActorMessage::Disconnect");
                    assert!(
                        self.socket.is_some(),
                        "Can't disconnect from socket server if there was no connection to begin with",
                    );
                    if let Some(socket) = self.socket.take() {
                        Self::unmount_event_handlers(&socket);
                        let _ = socket.close();
                    }
                }
            }
        }
    }

    fn mount_event_handlers(socket: &WebSocket) {
        let onmessage_callback = Closure::<dyn FnMut(_)>::new(move |message: MessageEvent| {
            // if let Ok(abuf) = message.data().dyn_into::<js_sys::ArrayBuffer>() {}
            // if let Ok(blob) = message.data().dyn_into::<web_sys::Blob>() {}
            // else { message.data() }
            if let Ok(txt) = message.data().dyn_into::<js_sys::JsString>() {
                Self::handle_received_message(txt);
            }
        });

        let onopen_callback = Closure::<dyn FnMut()>::new(move || {
            info!("Connected to socket server");
        });

        let onerror_callback = Closure::<dyn FnMut(_)>::new(move |e: ErrorEvent| {
            error!("{:#?}", e);
        });

        let onclose_callback = Closure::<dyn FnMut(_)>::new(move |e: CloseEvent| {
            error!("{:#?}", e);
            error!("Websocket server closed. Trying reconnecting in 5sec...");
            spawn(async move {
                wait_for(Duration::from_secs(5)).await;
                use_socket_handle().connect();
            });
        });

        socket.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));
        socket.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
        socket.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
        socket.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
        onmessage_callback.forget();
        onopen_callback.forget();
        onerror_callback.forget();
        onclose_callback.forget();
    }

    fn unmount_event_handlers(socket: &WebSocket) {
        let onmessage_callback = Closure::<dyn FnMut()>::new(|| ());
        let onopen_callback = Closure::<dyn FnMut()>::new(|| ());
        let onerror_callback = Closure::<dyn FnMut()>::new(|| ());
        let onclose_callback = Closure::<dyn FnMut()>::new(|| ());
        socket.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));
        socket.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
        socket.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
        socket.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
        onmessage_callback.forget();
        onopen_callback.forget();
        onerror_callback.forget();
        onclose_callback.forget();
    }
}
