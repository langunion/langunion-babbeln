use crate::actors::{use_http_handle, HttpError, HttpHandle};
use crate::core::models::User;
use crate::core::responses::{BlockUserResponse, GetBlockedUsersResponse, UnblockUserResponse};
use crate::core::task::spawn;
use log::{debug, error, info};
use reqwest::Method;
use serde_json::json;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static BLOCKED_USERS_HANDLE: LazyLock<BlockedUsersHandle> = LazyLock::new(|| {
    debug!("Created BLOCKED_USER_HANDLE");
    let (blocked_users_actor, blocked_users_handle) = BlockedUsersActor::new();
    BlockedUsersActor::run(blocked_users_actor);
    blocked_users_handle
});

pub fn use_blocked_users_handle() -> BlockedUsersHandle {
    BLOCKED_USERS_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetBlockedUsers(Vec<User>),
}

pub struct BlockedUsersActor {
    pub blocked_users: watch::Sender<Vec<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl BlockedUsersActor {
    pub fn new() -> (Self, BlockedUsersHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_blocked_users, watcher_blocked_users) = watch::channel::<Vec<User>>(Vec::new());

        let blocked_users_actor = Self {
            blocked_users: update_blocked_users,
            receiver,
        };

        let blocked_users_handle = BlockedUsersHandle::new(watcher_blocked_users, sender);

        (blocked_users_actor, blocked_users_handle)
    }

    pub fn run(blocked_users_actor: Self) {
        debug!("Started blocked user actor event loop");
        spawn(blocked_users_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    BlockedUsersActor::fetch();
                }
                ActorMessage::SetBlockedUsers(blocked_users) => {
                    debug!("ActorMessage::SetBlockedUsers");
                    let _ = self.blocked_users.send(blocked_users);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetBlockedUsersResponse>(
                Method::GET,
                "/users/blocked-users/",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_blocked_users_handle().set_blocked_users(response.result);
            }
        });
    }
}

#[derive(Clone)]
pub struct BlockedUsersHandle {
    pub blocked_users: watch::Receiver<Vec<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl BlockedUsersHandle {
    fn new(
        blocked_users: watch::Receiver<Vec<User>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            blocked_users,
            sender,
        }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_blocked_users(Vec::new());
    }

    pub fn blocked_users(&mut self) -> Vec<User> {
        self.blocked_users.borrow_and_update().clone()
    }

    pub fn set_blocked_users(&self, blocked_users: Vec<User>) {
        let _ = self
            .sender
            .send(ActorMessage::SetBlockedUsers(blocked_users));
    }

    pub fn is_blocked(&mut self, user: &User) -> bool {
        let blocked_user_ids: Vec<usize> = self
            .blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let is_blocked: bool = blocked_user_ids.contains(&user.id);
        is_blocked
    }

    /// Add user to the blocked user state
    pub fn add_blocked_user(&mut self, user: User) {
        let mut blocked_users: Vec<User> = self.blocked_users();
        if !blocked_users.contains(&user) {
            blocked_users.insert(0, user);
            self.set_blocked_users(blocked_users);
        }
    }

    /// Remove user from the blocked user state
    pub fn remove_blocked_user(&mut self, user: User) {
        let blocked_users: Vec<User> = self
            .blocked_users()
            .into_iter()
            .filter(|u| u.id != user.id)
            .collect();
        self.set_blocked_users(blocked_users);
    }

    pub fn block_user(user_id: usize) -> oneshot::Receiver<Result<BlockUserResponse, HttpError>> {
        use_http_handle().do_authenticated_request(
            Method::POST,
            "/users/blocked-users/",
            Some(json!({
                "user_id": user_id,
            })),
        )
    }

    pub fn unblock_user(
        user_id: usize,
    ) -> oneshot::Receiver<Result<UnblockUserResponse, HttpError>> {
        use_http_handle().do_authenticated_request(
            Method::DELETE,
            format!("/users/blocked-users/{user_id}/"),
            None,
        )
    }
}
