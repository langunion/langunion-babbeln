use crate::actors::{
    use_app_handle, use_blocked_users_handle, use_chat_partners_handle, use_friendrequests_handle,
    use_friends_handle, use_posts_handle, use_reset_handle, use_user_handle,
};
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::{RefreshTokensResponse, ValidateTokenResponse};
use crate::core::task::{spawn, wait_for};
use async_recursion::async_recursion;
use log::{debug, error, info};
use reqwest::{Client, ClientBuilder, Method, RequestBuilder, StatusCode};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::json;
use std::sync::LazyLock;
use std::time::Duration;
use tokio::sync::{mpsc, oneshot, watch};

pub static HTTP_HANDLE: LazyLock<HttpHandle> = LazyLock::new(|| {
    debug!("Created HTTP_HANDLE");
    let (http_actor, http_handle) = HttpActor::new();
    HttpActor::run(http_actor);
    http_handle
});

pub fn use_http_handle() -> HttpHandle {
    HTTP_HANDLE.clone()
}

#[derive(Serialize, Deserialize, Debug)]
pub struct HttpError {
    pub status: u16,
    pub message: String,
}

#[derive(Debug)]
enum ActorMessage {
    SetAccessToken(Option<String>),
    SetRefreshToken(Option<String>),
}

pub struct HttpActor {
    pub access_token: watch::Sender<Option<String>>,
    pub refresh_token: watch::Sender<Option<String>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl HttpActor {
    pub fn new() -> (Self, HttpHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_access_token, watcher_access_token) = watch::channel::<Option<String>>(None);
        let (update_refresh_token, watcher_refresh_token) = watch::channel::<Option<String>>(None);

        let http_actor = Self {
            access_token: update_access_token,
            refresh_token: update_refresh_token,
            receiver,
        };

        let http_handle = HttpHandle::new(watcher_access_token, watcher_refresh_token, sender);

        (http_actor, http_handle)
    }

    pub fn run(http_actor: Self) {
        debug!("Started HttpActor event loop");
        spawn(http_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetAccessToken(token) => {
                    debug!("ActorMessage::SetAccessToken");
                    let _ = self.access_token.send(token);
                }
                ActorMessage::SetRefreshToken(token) => {
                    debug!("ActorMessage::SetRefreshToken");
                    let _ = self.refresh_token.send(token);
                }
            }
        }
    }
}

#[derive(Clone)]
pub struct HttpHandle {
    pub access_token: watch::Receiver<Option<String>>,
    pub refresh_token: watch::Receiver<Option<String>>,
    pub base_url: String,
    pub client: Client,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl HttpHandle {
    fn new(
        access_token: watch::Receiver<Option<String>>,
        refresh_token: watch::Receiver<Option<String>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        #[cfg(debug_assertions)]
        let base_url: String = "http://localhost:4000/v1".to_string();
        #[cfg(not(debug_assertions))]
        let base_url: String = "https://server.langunion.com/v1".to_string();

        let client = Client::builder().build().unwrap();

        Self {
            access_token,
            refresh_token,
            base_url,
            client,
            sender,
        }
    }

    pub fn access_token(&mut self) -> Option<String> {
        self.access_token.borrow_and_update().clone()
    }

    pub fn set_access_token(&self, token: Option<String>) {
        debug!("HttpHandle set_access_token");
        // Set the access token also in the cookie
        // so that the browser can use that for the websocket connection route
        #[cfg(target_arch = "wasm32")]
        if let Some(token) = token.clone() {
            use web_sys::wasm_bindgen::JsCast;
            let bearer_cookie: String = format!("Bearer={token}");
            let document = gloo::utils::document().unchecked_into::<web_sys::HtmlDocument>();
            document.set_cookie(&bearer_cookie).unwrap();
        }
        let _ = self.sender.send(ActorMessage::SetAccessToken(token));
    }

    pub async fn set_access_token_async(&mut self, token: Option<String>) {
        let _ = self.access_token();
        self.set_access_token(token);
        self.access_token.changed().await.unwrap();
    }

    pub async fn access_token_async(&mut self) -> String {
        let _ = self.access_token.wait_for(|t| t.is_some()).await;
        self.access_token.borrow_and_update().clone().unwrap()
    }

    pub fn refresh_token(&mut self) -> Option<String> {
        self.refresh_token.borrow_and_update().clone()
    }

    pub async fn refresh_token_async(&mut self) -> String {
        let _ = self.refresh_token.wait_for(|t| t.is_some()).await;
        self.refresh_token.borrow_and_update().clone().unwrap()
    }

    pub fn set_refresh_token(&self, token: Option<String>) {
        debug!("HttpHandle set_refresh_token");
        let _ = self.sender.send(ActorMessage::SetRefreshToken(token));
    }

    pub async fn set_refresh_token_async(&mut self, token: Option<String>) {
        let _ = self.refresh_token();
        self.set_refresh_token(token);
        self.refresh_token.changed().await.unwrap();
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_access_token(None);
        self.set_refresh_token(None);
    }

    pub fn fetch_all() {
        debug!("Fetch all actors state");
        spawn(async {
            use_user_handle().fetch();
            use_blocked_users_handle().fetch();
            use_chat_partners_handle().fetch();
            use_friendrequests_handle().fetch();
            use_friends_handle().fetch();
            use_posts_handle().fetch();
            // use_messages_handle().fetch();
            use_app_handle().request_repaint();
        });
    }
}

impl HttpHandle {
    pub fn do_request<TResponse>(
        &mut self,
        method: Method,
        route: impl Into<String>,
        body: Option<serde_json::Value>,
    ) -> oneshot::Receiver<Result<TResponse, HttpError>>
    where
        TResponse: DeserializeOwned + Send + 'static,
    {
        let (sender, receiver) = oneshot::channel::<Result<TResponse, HttpError>>();
        let route: String = {
            let route: String = route.into();
            let route: String = match route.starts_with(&self.base_url) {
                true => route,
                false => format!("{}{route}", &self.base_url),
            };
            route
        };
        info!("Executing HTTP request to {route}");
        let request: RequestBuilder = {
            let mut req = self.client.request(method, &route);
            if let Some(ref body) = body {
                req = req.json(body);
            }
            req
        };

        spawn(async move {
            let response = request.send().await;
            let response: Result<TResponse, HttpError> =
                HttpHandle::convert_response(response).await;
            let _ = sender.send(response);
            use_app_handle().request_repaint();
        });
        receiver
    }

    pub fn do_authenticated_request<TResponse>(
        &mut self,
        method: Method,
        route: impl Into<String>,
        body: Option<serde_json::Value>,
    ) -> oneshot::Receiver<Result<TResponse, HttpError>>
    where
        TResponse: DeserializeOwned + Send + 'static,
    {
        let (sender, receiver) = oneshot::channel::<Result<TResponse, HttpError>>();
        let route: String = {
            let route: String = route.into();
            let route: String = match route.starts_with(&self.base_url) {
                true => route,
                false => format!("{}{route}", &self.base_url),
            };
            route
        };
        info!("Executing authenticated HTTP request to {route}");
        let bearer_token: String = self.access_token().expect(&format!(
            "No Bearer token found. Cannot execute authenticated request to {route}"
        ));
        let request: RequestBuilder = {
            let mut req = self
                .client
                .request(method, &route)
                .bearer_auth(bearer_token);
            if let Some(ref body) = body {
                req = req.json(body);
            }
            req
        };

        spawn(async move {
            let mut http_handle = use_http_handle();
            let response = {
                let res = request.try_clone().unwrap().send().await;
                HttpHandle::convert_response::<TResponse>(res).await
            };

            match response {
                Err(http_error) => {
                    if http_error.status == StatusCode::UNAUTHORIZED {
                        let refresh_token = http_handle.refresh_token().expect(
                            "No refresh token found. Cannot try to refresh authentication tokens",
                        );
                        let refresh_response: Result<RefreshTokensResponse, HttpError> =
                            http_handle
                                .do_request(
                                    Method::POST,
                                    "/tokens/refresh/",
                                    Some(json!({
                                        "refresh": refresh_token,
                                    })),
                                )
                                .await
                                .unwrap();
                        let final_response = match refresh_response {
                            Ok(refresh_response) => {
                                let redo_request: RequestBuilder =
                                    request.bearer_auth(&refresh_response.access);
                                http_handle.set_access_token(Some(refresh_response.access));
                                http_handle.set_refresh_token(Some(refresh_response.refresh));
                                let redo_response = redo_request.try_clone().unwrap().send().await;
                                HttpHandle::convert_response::<TResponse>(redo_response).await
                            }
                            Err(http_error) => {
                                error!(
                                    "Refresh authentication tokens failed: {}",
                                    &http_error.message
                                );
                                use_reset_handle().reset();
                                Err(http_error)
                            }
                        };
                        let _ = sender.send(final_response);
                    }
                }
                Ok(_) => {
                    let _ = sender.send(response);
                }
            }
            use_app_handle().request_repaint();
        });
        receiver
    }

    pub async fn maybe_reauthenticate(&mut self) {
        let receiver = self.do_authenticated_request::<ValidateTokenResponse>(
            reqwest::Method::PATCH,
            "/tokens/validate/",
            None,
        );
        let _ = receiver.await;
    }
}

impl HttpHandle {
    /// Converts a reqwest::Result response into the data of the response or into an HttpError
    pub async fn convert_response<TResponse: DeserializeOwned>(
        result: Result<reqwest::Response, reqwest::Error>,
    ) -> Result<TResponse, HttpError> {
        match result {
            Ok(response) => {
                info!("{} :: {}", response.status(), response.url());
                match response.status() {
                    // Depending on the statuscode we know when the response is of type
                    // TResponse or HttpError
                    StatusCode::OK | StatusCode::CREATED => {
                        let response: TResponse = response
                            .json()
                            .await
                            .expect("Could not convert 'response' into 'TResponse'");
                        Ok(response)
                    }
                    _ => {
                        let http_error: HttpError = response
                            .json()
                            .await
                            .expect("Could not convert 'reponse' into 'HttpError'");
                        error!("{}", &http_error.message);
                        Err(http_error)
                    }
                }
            }
            Err(err) => {
                error!("Network Error: {err}");
                Err(HttpError {
                    status: 0,
                    message: format!("Network Error: No internet connection"),
                })
            }
        }
    }

    /// Simple synchronous non-blocking helper function to check if a
    /// oneshot channel's receiver that expects a HttpError on error
    /// Use this inside eframe's update (render) loop
    /// This method is used to make handling http requests simpler
    pub fn handle_response_data<TResponse>(
        response: &mut Option<oneshot::Receiver<Result<TResponse, HttpError>>>,
        on_success: impl FnOnce(TResponse),
        on_error: impl FnOnce(HttpError),
    ) {
        if let Some(receiver) = response {
            match receiver.try_recv() {
                Ok(response) => {
                    match response {
                        Ok(data) => {
                            on_success(data);
                        }
                        Err(err) => {
                            on_error(err);
                        }
                    };
                    use_app_handle().request_repaint();
                }
                Err(oneshot::error::TryRecvError::Closed) => *response = None,
                Err(oneshot::error::TryRecvError::Empty) => (),
            }
        }
    }
}
