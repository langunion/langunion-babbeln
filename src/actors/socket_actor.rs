#[cfg(not(target_arch = "wasm32"))]
pub use crate::actors::socket_actor_native::{ActorMessage, SocketActor};
#[cfg(target_arch = "wasm32")]
pub use crate::actors::socket_actor_wasm::{ActorMessage, SocketActor};
use crate::actors::{
    use_blocked_users_handle, use_friendrequests_handle, use_friends_handle, use_http_handle,
    use_messages_handle, use_posts_handle, use_reset_handle, use_user_handle, HttpHandle,
};
use crate::core::models;
use crate::core::socket_events::{SocketAction, SocketEvent};
use crate::core::task::spawn;
use crate::core::webrtc_events::WebRtcEvent;
use log::{debug, info};
use std::sync::LazyLock;
use tokio::sync::mpsc;

pub static SOCKET_HANDLE: LazyLock<SocketHandle> = LazyLock::new(|| {
    debug!("Created SOCKET_HANDLE");
    let (socket_actor, socket_handle) = SocketActor::new();
    SocketActor::run(socket_actor);
    socket_handle
});

pub fn use_socket_handle() -> SocketHandle {
    SOCKET_HANDLE.clone()
}

pub trait HandleSocketEvents {
    fn handle_received_message(msg: impl Into<String>) {
        let msg: String = msg.into();
        if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&msg) {
            Self::handle_socket_event(socket_event);
        } else if let Ok(webrtc_event) = serde_json::from_str::<WebRtcEvent>(&msg) {
            Self::handle_webrtc_event(webrtc_event);
        }
    }

    fn handle_socket_event(socket_event: SocketEvent) {
        debug!("{:?}: {}", socket_event.action, socket_event.message);
        match socket_event.action {
            SocketAction::DeleteUser => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    let mut user_handle = use_user_handle();
                    let mut friends_handle = use_friends_handle();
                    let me = user_handle.me();
                    if user.id == me.id {
                        use_reset_handle().reset();
                    } else {
                        friends_handle.remove_friend(user);
                    }
                }
            }
            SocketAction::UpdateUser | SocketAction::UpdateUserAvatar => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    let mut user_handle = use_user_handle();
                    let mut friends_handle = use_friends_handle();
                    let me = user_handle.me();
                    if user.id == me.id {
                        user_handle.set_me(Some(user));
                    } else {
                        friends_handle.update_friend(user);
                    }
                }
            }
            SocketAction::UserWentOffline | SocketAction::UserWentOnline => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    use_friends_handle().update_friend(user);
                }
            }
            SocketAction::RemoveFriend => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    use_friends_handle().remove_friend(user);
                }
            }
            SocketAction::BlockUser => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    use_blocked_users_handle().add_blocked_user(user);
                }
            }
            SocketAction::UnblockUser => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    use_blocked_users_handle().remove_blocked_user(user);
                }
            }
            // Tokens
            SocketAction::ResetTokens => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    let me = use_user_handle().me();
                    if user.id == me.id {
                        use_reset_handle().reset();
                    }
                }
            }
            // Friendrequests
            SocketAction::SendFriendrequest => {
                if let Some(friendrequest) = socket_event.parse_payload::<models::Friendrequest>() {
                    use_friendrequests_handle().add_friendrequest(friendrequest);
                }
            }
            SocketAction::AcceptFriendrequest => {
                if let Some(friend) = socket_event.parse_payload::<models::User>() {
                    use_friendrequests_handle().remove_friendrequest_with_user(friend.clone());
                    use_friends_handle().add_friend(friend);
                }
            }
            SocketAction::DeclineFriendrequest => {
                if let Some(friend) = socket_event.parse_payload::<models::User>() {
                    use_friendrequests_handle().remove_friendrequest_with_user(friend);
                }
            }
            // Posts
            SocketAction::CreatePost => {
                if let Some(post) = socket_event.parse_payload::<models::Post>() {
                    use_posts_handle().add_post(post);
                }
            }
            SocketAction::RemovePost => {
                if let Some(post) = socket_event.parse_payload::<models::Post>() {
                    use_posts_handle().remove_post(post);
                }
            }
            SocketAction::LikePost
            | SocketAction::UnlikePost
            | SocketAction::CommentPost
            | SocketAction::RemoveCommentPost => {
                if let Some(post) = socket_event.parse_payload::<models::Post>() {
                    use_posts_handle().update_post(post);
                }
            }
            // Message
            SocketAction::SendMessage => {
                if let Some(message) = socket_event.parse_payload::<models::Message>() {
                    use_messages_handle().add_message(message);
                }
            }
            SocketAction::UpdateMessage => {
                if let Some(message) = socket_event.parse_payload::<models::Message>() {
                    use_messages_handle().update_message(message);
                }
            }
            SocketAction::DeleteMessage | SocketAction::DeleteMessagePrivately => {
                if let Some(message) = socket_event.parse_payload::<models::Message>() {
                    use_messages_handle().remove_message(message);
                }
            }
            SocketAction::DeleteChat => {
                if let Some(user) = socket_event.parse_payload::<models::User>() {
                    use_messages_handle().remove_chat(user);
                }
            }
        }
    }

    fn handle_webrtc_event(webrtc_event: WebRtcEvent) {
        debug!("{:?}", webrtc_event.action);
    }
}

#[derive(Clone)]
pub struct SocketHandle {
    pub sender: mpsc::UnboundedSender<ActorMessage>,
}

impl SocketHandle {
    pub fn new(sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { sender }
    }

    /// Create a new connection to the socket server
    /// If a connection is already established it'll get closed
    pub fn connect(&self) {
        let sender = self.sender.clone();
        spawn(async move {
            // Make sure that the client is authenticated
            // before trying to connect to the socket server
            use_http_handle().maybe_reauthenticate().await; // TODO inline this? where do i need this once more?
            use_http_handle().access_token_async().await;
            use_http_handle().refresh_token_async().await;
            let _ = sender.send(ActorMessage::Connect);
        });
    }

    pub async fn is_connected(&self) -> bool {
        todo!()
    }

    pub fn disconnect(&self) {
        let _ = self.sender.send(ActorMessage::Disconnect);
    }

    pub async fn is_disconnected(&self) -> bool {
        todo!()
    }
}
