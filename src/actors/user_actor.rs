use crate::actors::{
    use_blocked_users_handle, use_friends_handle, use_http_handle, HttpError, HttpHandle,
};
use crate::core::models::User;
use crate::core::responses::GetUserResponse;
use crate::core::task::spawn;
use log::{debug, error, info, warn};
use regex::Regex;
use reqwest::Method;
use std::str::FromStr;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static USER_HANDLE: LazyLock<UserHandle> = LazyLock::new(|| {
    debug!("Created USER_HANDLE");
    let (user_actor, user_handle) = UserActor::new();
    UserActor::run(user_actor);
    user_handle
});

pub fn use_user_handle() -> UserHandle {
    USER_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetMe(Option<User>),
}

pub struct UserActor {
    pub me: watch::Sender<Option<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl UserActor {
    pub fn new() -> (Self, UserHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_me, watcher_me) = watch::channel::<Option<User>>(None);

        let user_actor = Self {
            me: update_me,
            receiver,
        };

        let user_handle = UserHandle::new(watcher_me, sender);

        (user_actor, user_handle)
    }

    pub fn run(user_actor: Self) {
        debug!("Started user actor event loop");
        spawn(user_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    UserActor::fetch();
                }
                ActorMessage::SetMe(me) => {
                    debug!("ActorMessage::SetMe");
                    let _ = self.me.send(me);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetUserResponse>(
                Method::GET,
                "/users/me",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_user_handle().set_me(Some(response.result));
            }
        });
    }
}

#[derive(Clone)]
pub struct UserHandle {
    pub me: watch::Receiver<Option<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl UserHandle {
    fn new(me: watch::Receiver<Option<User>>, sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { me, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_me(None);
    }

    /// Get the currently logged in user.
    /// If the user doesn't exist then the app will panic
    pub fn me(&mut self) -> User {
        self.me
            .borrow_and_update()
            .clone()
            .expect("The client should be logged in when calling 'me()'")
    }

    /// Try to get the currently logged in user.
    pub fn try_me(&mut self) -> Option<User> {
        self.me.borrow_and_update().clone()
    }

    /// Get the currently logged in user asynchrously
    pub async fn me_async(&mut self) -> User {
        let _ = self.me.wait_for(|me| me.is_some()).await;
        self.me.borrow_and_update().clone().unwrap()
    }

    pub fn set_me(&self, me: Option<User>) {
        let _ = self.sender.send(ActorMessage::SetMe(me));
    }

    pub fn get_user_by_id(user_id: usize) -> Option<User> {
        let mut user_handle = use_user_handle();
        let mut friends_handle = use_friends_handle();
        let friends: Vec<User> = friends_handle.friends();

        if let Some(me) = user_handle.try_me() {
            if me.id == user_id {
                return Some(me);
            }
        }

        for user in friends {
            if user.id == user_id {
                return Some(user);
            }
        }

        None
    }

    pub fn get_user_by_url(url: impl Into<String>) -> Option<User> {
        let url: String = url.into();
        let user_id: Option<usize> = Self::get_user_id_by_url(url);
        match user_id {
            Some(user_id) => UserHandle::get_user_by_id(user_id),
            None => None,
        }
    }

    pub fn get_user_id_by_url(url: impl Into<String>) -> Option<usize> {
        let url: String = url.into();
        let regex_is_user_url =
            Regex::new(r"^https?:\/\/[\w,\d,.]*(:\d+)?\/v\d+\/users\/\d+\/?$").unwrap();
        let regex_get_user_id = Regex::new(r"users\/\d+\/?$").unwrap();
        if !regex_is_user_url.is_match(&url) {
            warn!("The provided url does not match a typical users url");
            return None;
        }
        if let Some(mat) = regex_get_user_id.find(&url) {
            let mat: String = String::from(mat.as_str());
            let mat: Vec<&str> = mat.rsplit("/").collect();
            return usize::from_str(mat[1]).ok();
        }
        None
    }

    pub fn display_username(user: &User) -> String {
        if let Some(me) = use_user_handle().try_me() {
            let is_me: bool = user.id == me.id;
            if is_me {
                return user.username.clone();
            }
        }

        let is_blocked: bool = use_blocked_users_handle().is_blocked(user);
        if is_blocked {
            return "blocked user".to_string();
        }

        let is_friend: bool = use_friends_handle().is_friend(user);
        if is_friend {
            return user.username.clone();
        }

        "unknown".to_string()
    }

    // TODO remove these two methods
    pub fn display_username_by_id(user_id: usize) -> String {
        let user: Option<User> = UserHandle::get_user_by_id(user_id);

        if let Some(user) = user {
            return UserHandle::display_username(&user);
        }

        "unknown".to_string()
    }

    pub fn display_username_by_url(url: impl Into<String>) -> String {
        let url: String = url.into();
        let user: Option<User> = UserHandle::get_user_by_url(url);

        if let Some(user) = user {
            return UserHandle::display_username(&user);
        }

        "unknown".to_string()
    }
}
