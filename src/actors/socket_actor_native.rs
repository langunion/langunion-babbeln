use crate::actors::{
    socket_actor::HandleSocketEvents, use_app_handle, use_http_handle, use_socket_handle,
    SocketHandle,
};
use crate::core::task::{spawn, wait_for};
use async_recursion::async_recursion;
use futures_util::SinkExt;
use futures_util::{
    stream::{SplitSink, SplitStream},
    StreamExt,
};
use log::{debug, error, info};
use std::time::Duration;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio_tungstenite::{
    tungstenite::http::{header, Request},
    tungstenite::protocol::Message,
    MaybeTlsStream, WebSocketStream,
};

type StreamWrite = SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>;
type StreamRead = SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>;

#[derive(Debug)]
pub enum ActorMessage {
    SetSocket((StreamWrite, StreamRead)),
    Connect,
    Disconnect,
}

pub struct SocketActor {
    socket: (Option<StreamWrite>, Option<StreamRead>),
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl HandleSocketEvents for SocketActor {}

impl SocketActor {
    pub fn new() -> (Self, SocketHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();
        let socket_actor = Self {
            socket: (None, None),
            receiver,
        };
        let socket_handle = SocketHandle::new(sender);
        (socket_actor, socket_handle)
    }

    pub fn run(socket_actor: Self) {
        debug!("Started SocketActor event loop");
        spawn(socket_actor.event_loop());
    }

    pub fn connect() {
        spawn(async move {
            debug!("Connect to socket server");
            let host: String = "localhost:4000".to_string();
            let uri: String = format!("ws://{host}/v1/ws/");
            let mut http_handle = use_http_handle();
            let access_token: String = http_handle.access_token_async().await;
            let request: Request<()> = Request::builder()
                .uri(uri)
                .header(header::HOST, host)
                .header(header::AUTHORIZATION, &format!("Bearer {}", &access_token))
                .header(header::UPGRADE, "websocket")
                .header(header::CONNECTION, "Upgrade")
                .header(header::SEC_WEBSOCKET_KEY, "pDTDUR/eEFpXHb847ddH1g==")
                .header(header::SEC_WEBSOCKET_VERSION, "13")
                .body(())
                .unwrap();

            #[async_recursion]
            async fn connect_to_socket(request: Request<()>) {
                let request_cloned = request.clone();
                match tokio_tungstenite::connect_async(request).await {
                    Ok((socket, _)) => {
                        let (write, read) = socket.split();

                        let _ = use_socket_handle()
                            .sender
                            .send(ActorMessage::SetSocket((write, read)));
                        info!("Connected to socket server");
                        use_app_handle().request_repaint();
                    }
                    Err(e) => {
                        error!("{e}");
                        error!("Failed to connect to websocket server. Trying again in 5sec...");
                        wait_for(Duration::from_secs(5)).await;
                        connect_to_socket(request_cloned).await;
                    }
                }
            }

            connect_to_socket(request).await;
        });
    }

    fn disconnect(write: Option<StreamWrite>, read_handle: Option<tokio::task::JoinHandle<()>>) {
        info!("Disconnected from socket connection");
        if let Some(mut write) = write {
            let _ = write.close();
        }
        if let Some(read_handle) = read_handle {
            read_handle.abort();
        }
    }

    async fn event_loop(mut self) {
        let mut read_handle: Option<tokio::task::JoinHandle<()>> = None;
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetSocket((write, read)) => {
                    debug!("ActorMessage::SetSocket");
                    self.socket = (Some(write), Some(read));
                    read_handle = Some(tokio::spawn(async move {
                        Self::socket_event_loop(self.socket.1.unwrap()).await;
                    }));
                }
                ActorMessage::Connect => {
                    debug!("ActorMessage::Connect");
                    assert!(
                        self.socket.0.is_none(),
                        "Can't connect to socket server if there is still a pending connection. Call 'disconnect' first",
                    );
                    Self::connect();
                }
                ActorMessage::Disconnect => {
                    debug!("ActorMessage::Disconnect");
                    assert!(
                        self.socket.0.is_some(),
                        "Can't disconnect from socket server if there was no connection to begin with",
                    );
                    Self::disconnect(self.socket.0.take(), read_handle.take());
                }
            }
        }
    }

    async fn socket_event_loop(stream_read: StreamRead) {
        debug!("Started socket event loop");
        stream_read
            .for_each(|message| async {
                match message {
                    Ok(Message::Text(txt)) => {
                        SocketActor::handle_received_message(txt);
                    }
                    Ok(Message::Close(e)) => {
                        error!("Socket close happened: {:?}", e);
                        let socket_handle = use_socket_handle();
                        socket_handle.disconnect();
                        socket_handle.connect();
                    }
                    Err(e) => {
                        error!("Socket Error: {e}");
                        let socket_handle = use_socket_handle();
                        socket_handle.disconnect();
                        socket_handle.connect();
                    }
                    // Ok(Message::Binary(bin)) => println!("Received binary data"),
                    // Ok(Message::Ping(_)) => println!("Received ping"),
                    // Ok(Message::Pong(_)) => println!("Received pong"),
                    _ => (), // Handle other message types as needed
                }
                use_app_handle().request_repaint();
            })
            .await;
    }
}
