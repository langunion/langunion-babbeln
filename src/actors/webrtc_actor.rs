#[cfg(not(target_arch = "wasm32"))]
use crate::actors::webrtc_actor_native::WebrtcActor;
#[cfg(target_arch = "wasm32")]
use crate::actors::webrtc_actor_wasm::WebrtcActor;
use bytes::Bytes;
use chrono::{DateTime, Utc};
use image::{ImageBuffer, Rgba};
use log::{debug, info};
use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::ptr;
use std::sync::LazyLock;
use std::sync::Once;
use tokio::sync::{broadcast, mpsc, watch};

static INIT: Once = Once::new();
static mut MEDIA_STREAM_CHANNELS: *mut HashMap<usize, MediaStreamChannel> = ptr::null_mut();
fn use_media_stream_channels() -> &'static mut HashMap<usize, MediaStreamChannel> {
    unsafe {
        INIT.call_once(|| {
            MEDIA_STREAM_CHANNELS = Box::into_raw(Box::new(HashMap::new()));
        });
        &mut *MEDIA_STREAM_CHANNELS
    }
}

pub static WEBRTC_HANDLE: LazyLock<WebrtcHandle> = LazyLock::new(|| {
    debug!("Created WEBRTC_HANDLE");
    let (webrtc_actor, webrtc_handle) = WebrtcActor::new();
    WebrtcActor::run(webrtc_actor);
    webrtc_handle
});

pub fn use_webrtc_handle() -> WebrtcHandle {
    WEBRTC_HANDLE.clone()
}

#[derive(Debug)]
pub enum ActorMessage {
    Reset,
    StartCall,
    SetCallKey(Option<String>),
    EnableWebcam,
    DisableWebcam,
    EnableMicrophone,
    DisableMicrophone,
    EnableScreenshare,
    DisableSceenshare,
}

#[derive(Clone)]
pub struct WebrtcHandle {
    call_key: watch::Receiver<Option<String>>,
    is_in_call: watch::Receiver<bool>,
    is_webcam_on: watch::Receiver<bool>,
    is_microphone_on: watch::Receiver<bool>,
    is_screenshare_on: watch::Receiver<bool>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

pub type Frame = ImageBuffer<Rgba<u8>, Vec<u8>>;
pub struct MediaStreamChannel {
    pub video: (broadcast::Sender<Frame>, broadcast::Receiver<Frame>),
    pub audio: (broadcast::Sender<Frame>, broadcast::Receiver<Frame>),
}

impl MediaStreamChannel {
    pub fn new() -> Self {
        let limit = 1;
        Self {
            video: broadcast::channel::<Frame>(limit),
            audio: broadcast::channel::<Frame>(limit),
        }
    }
}

impl Clone for MediaStreamChannel {
    fn clone(&self) -> Self {
        Self {
            video: (self.video.0.clone(), self.video.0.subscribe()),
            audio: (self.audio.0.clone(), self.audio.0.subscribe()),
        }
    }
}

impl WebrtcHandle {
    pub fn new(
        call_key: watch::Receiver<Option<String>>,
        is_in_call: watch::Receiver<bool>,
        is_webcam_on: watch::Receiver<bool>,
        is_microphone_on: watch::Receiver<bool>,
        is_screenshare_on: watch::Receiver<bool>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            call_key,
            is_in_call,
            is_webcam_on,
            is_microphone_on,
            is_screenshare_on,
            sender,
        }
    }

    pub fn media_stream_channel(&self, user_id: usize) -> &mut MediaStreamChannel {
        let streams = use_media_stream_channels();
        match streams.contains_key(&user_id) {
            true => streams.get_mut(&user_id).unwrap(),
            false => {
                let m = MediaStreamChannel::new();
                streams.insert(user_id, m);
                streams.get_mut(&user_id).unwrap()
            }
        }
    }

    pub fn start_call(&self) {
        let _ = self.sender.send(ActorMessage::StartCall);
    }

    pub fn stop_call(&self) {
        let _ = self.sender.send(ActorMessage::Reset);
    }
}
