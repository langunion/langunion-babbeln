use crate::actors::{use_blocked_users_handle, use_http_handle, HttpError, HttpHandle};
use crate::core::models::User;
use crate::core::responses::{GetFriendsResponse, RemoveFriendResponse};
use crate::core::task::spawn;
use log::{debug, error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static FRIENDS_HANDLE: LazyLock<FriendsHandle> = LazyLock::new(|| {
    debug!("Created FRIENDS_HANDLE");
    let (friends_actor, friends_handle) = FriendsActor::new();
    FriendsActor::run(friends_actor);
    friends_handle
});

pub fn use_friends_handle() -> FriendsHandle {
    FRIENDS_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetFriends(Vec<User>),
}

pub struct FriendsActor {
    pub friends: watch::Sender<Vec<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl FriendsActor {
    pub fn new() -> (Self, FriendsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_friends, watcher_friends) = watch::channel::<Vec<User>>(Vec::new());

        let friends_actor = Self {
            friends: update_friends,
            receiver,
        };

        let friends_handle = FriendsHandle::new(watcher_friends, sender);

        (friends_actor, friends_handle)
    }

    pub fn run(friends_actor: Self) {
        debug!("Started friends actor event loop");
        spawn(friends_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    FriendsActor::fetch();
                }
                ActorMessage::SetFriends(friends) => {
                    debug!("ActorMessage::SetFriends");
                    let _ = self.friends.send(friends);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetFriendsResponse>(
                Method::GET,
                "/users/friends/",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_friends_handle().set_friends(response.result);
            }
        });
    }
}

#[derive(Clone)]
pub struct FriendsHandle {
    pub friends: watch::Receiver<Vec<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl FriendsHandle {
    fn new(
        friends: watch::Receiver<Vec<User>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self { friends, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_friends(Vec::new());
    }

    pub fn friends(&mut self) -> Vec<User> {
        self.friends.borrow_and_update().clone()
    }

    pub fn set_friends(&self, friends: Vec<User>) {
        let _ = self.sender.send(ActorMessage::SetFriends(friends));
    }

    pub fn friends_filtered_blocked(&mut self) -> Vec<User> {
        let friends: Vec<User> = self.friends();
        let blocked_user_ids: Vec<usize> = use_blocked_users_handle()
            .blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let filtered_friends: Vec<User> = friends
            .into_iter()
            .filter(|user: &User| !blocked_user_ids.contains(&user.id))
            .collect();

        filtered_friends
    }

    pub fn is_friend(&mut self, user: &User) -> bool {
        let friend_user_ids: Vec<usize> = self
            .friends()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let is_friend: bool = friend_user_ids.contains(&user.id);
        is_friend
    }

    /// Add a friend to the friends state
    pub fn add_friend(&mut self, user: User) {
        let mut friends: Vec<User> = self.friends();
        if !friends.contains(&user) {
            friends.insert(0, user);
            self.set_friends(friends);
        }
    }

    /// Remove a friend from the friends state
    pub fn remove_friend(&mut self, user: User) {
        let friends: Vec<User> = self
            .friends()
            .into_iter()
            .filter(|f| f.id != user.id)
            .collect();
        self.set_friends(friends);
    }

    /// Update a friend stored inside the friends state
    pub fn update_friend(&mut self, user: User) {
        let mut friends: Vec<User> = self.friends();
        if let Some(idx) = friends.iter().position(|f| f.id == user.id) {
            friends[idx] = user;
            self.set_friends(friends);
        }
    }

    pub fn unfriend_user(
        user_id: usize,
    ) -> oneshot::Receiver<Result<RemoveFriendResponse, HttpError>> {
        use_http_handle().do_authenticated_request(
            Method::DELETE,
            format!("/users/friends/{user_id}/"),
            None,
        )
    }
}
