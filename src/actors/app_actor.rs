use crate::actors::{
    use_blocked_users_handle, use_chat_partners_handle, use_friendrequests_handle,
    use_friends_handle, use_http_handle, use_messages_handle, use_posts_handle, use_socket_handle,
    use_user_handle,
};
use crate::core::task::spawn;
use eframe::egui::Context;
use log::{debug, info};
use std::ptr;
use std::sync::LazyLock;
use std::sync::Once;
use tokio::sync::{mpsc, watch};

static INIT: Once = Once::new();
static mut RESET_HANDLE: *mut ResetHandle = ptr::null_mut();

pub fn use_reset_handle() -> &'static mut ResetHandle {
    unsafe {
        INIT.call_once(|| {
            debug!("Created RESET_HANDLE");
            let (sender, receiver) = mpsc::unbounded_channel::<()>();
            RESET_HANDLE = Box::into_raw(Box::new(ResetHandle { sender, receiver }));
        });
        &mut *RESET_HANDLE
    }
}

pub struct ResetHandle {
    sender: mpsc::UnboundedSender<()>,
    pub receiver: mpsc::UnboundedReceiver<()>,
}

impl ResetHandle {
    pub fn reset(&self) {
        debug!("Reset all actors state");
        use_socket_handle().disconnect();
        use_http_handle().reset_state();
        use_blocked_users_handle().reset_state();
        use_chat_partners_handle().reset_state();
        use_friendrequests_handle().reset_state();
        use_friends_handle().reset_state();
        use_messages_handle().reset_state();
        use_posts_handle().reset_state();
        use_user_handle().reset_state();
        use_app_handle().reset_state();
        // Reset GUI
        let _ = self.sender.send(());
        use_app_handle().request_repaint();
    }
}

pub static APP_HANDLE: LazyLock<AppHandle> = LazyLock::new(|| {
    debug!("Created APP_HANDLE");
    let (app_actor, app_handle) = AppActor::new();
    AppActor::run(app_actor);
    app_handle
});

pub fn use_app_handle() -> AppHandle {
    APP_HANDLE.clone()
}

#[derive(Clone, Copy, Debug)]
pub enum Window {
    Loading,
    Login,
    Main,
}

#[derive(Debug)]
enum ActorMessage {
    SetCtx(Context),
    SetShowDashboard,
    SetShowFriends,
    SetShowTimeline,
    SetShowChat(usize),
    SetShowWindow(Window),
}

pub struct AppActor {
    pub ctx: watch::Sender<Option<Context>>,
    pub show_dashboard: watch::Sender<bool>,
    pub show_friends: watch::Sender<bool>,
    pub show_timeline: watch::Sender<bool>,
    pub show_chat: watch::Sender<Option<usize>>,
    pub show_window: watch::Sender<Window>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl AppActor {
    pub fn new() -> (Self, AppHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_ctx, watcher_ctx) = watch::channel::<Option<Context>>(None);
        let (update_show_dashboard, watcher_show_dashboard) = watch::channel::<bool>(true);
        let (update_show_friends, watcher_show_friends) = watch::channel::<bool>(false);
        let (update_show_timeline, watcher_show_timeline) = watch::channel::<bool>(false);
        let (update_show_chat, watcher_show_chat) = watch::channel::<Option<usize>>(None);
        let (update_show_window, watcher_show_window) = watch::channel::<Window>(Window::Loading);

        let app_actor = Self {
            ctx: update_ctx,
            show_dashboard: update_show_dashboard,
            show_friends: update_show_friends,
            show_timeline: update_show_timeline,
            show_chat: update_show_chat,
            show_window: update_show_window,
            receiver,
        };

        let app_handle = AppHandle::new(
            sender,
            watcher_ctx,
            watcher_show_dashboard,
            watcher_show_friends,
            watcher_show_timeline,
            watcher_show_chat,
            watcher_show_window,
        );

        (app_actor, app_handle)
    }

    pub fn run(app_actor: Self) {
        debug!("Started AppActor event loop");
        spawn(app_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetCtx(ctx) => {
                    let _ = self.ctx.send(Some(ctx));
                }
                ActorMessage::SetShowDashboard => {
                    debug!("ActorMessage::SetShowDashboard");
                    let _ = self.show_dashboard.send(true);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowFriends => {
                    debug!("ActorMessage::SetShowFriends");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(true);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowTimeline => {
                    debug!("ActorMessage::SetShowTimeline");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(true);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowChat(user_id) => {
                    debug!("ActorMessage::SetShowChat");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(Some(user_id));
                }
                ActorMessage::SetShowWindow(window) => {
                    debug!("ActorMessage::SetShowWindow({window:?})");
                    let _ = self.show_window.send(window);
                }
            }
        }
    }
}

#[derive(Clone)]
pub struct AppHandle {
    pub ctx: watch::Receiver<Option<Context>>,
    pub show_dashboard: watch::Receiver<bool>,
    pub show_friends: watch::Receiver<bool>,
    pub show_timeline: watch::Receiver<bool>,
    pub show_chat: watch::Receiver<Option<usize>>,
    pub show_window: watch::Receiver<Window>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl AppHandle {
    fn new(
        sender: mpsc::UnboundedSender<ActorMessage>,
        ctx: watch::Receiver<Option<Context>>,
        show_dashboard: watch::Receiver<bool>,
        show_friends: watch::Receiver<bool>,
        show_timeline: watch::Receiver<bool>,
        show_chat: watch::Receiver<Option<usize>>,
        show_window: watch::Receiver<Window>,
    ) -> Self {
        Self {
            sender,
            ctx,
            show_dashboard,
            show_friends,
            show_timeline,
            show_chat,
            show_window,
        }
    }

    pub fn ctx(&mut self) -> Option<Context> {
        self.ctx.borrow_and_update().clone()
    }

    pub fn set_ctx(&self, ctx: Context) {
        let _ = self.sender.send(ActorMessage::SetCtx(ctx));
    }

    pub fn show_dashboard(&mut self) -> bool {
        *self.show_dashboard.borrow_and_update()
    }

    pub fn set_show_dashboard(&self) {
        let _ = self.sender.send(ActorMessage::SetShowDashboard);
    }

    pub fn show_friends(&mut self) -> bool {
        *self.show_friends.borrow_and_update()
    }

    pub fn set_show_friends(&self) {
        let _ = self.sender.send(ActorMessage::SetShowFriends);
    }

    pub fn show_timeline(&mut self) -> bool {
        *self.show_timeline.borrow_and_update()
    }

    pub fn set_show_timeline(&self) {
        let _ = self.sender.send(ActorMessage::SetShowTimeline);
    }

    pub fn show_chat(&mut self) -> Option<usize> {
        *self.show_chat.borrow_and_update()
    }

    pub fn set_show_chat(&self, user_id: usize) {
        let _ = self.sender.send(ActorMessage::SetShowChat(user_id));
    }

    pub fn window(&mut self) -> Window {
        *self.show_window.borrow_and_update()
    }

    pub fn set_window(&self, window: Window) {
        let _ = self.sender.send(ActorMessage::SetShowWindow(window));
    }

    pub fn reset_state(&mut self) {
        debug!("Resetted state");
        self.set_show_dashboard();
        self.set_window(Window::Loading);
    }

    pub fn request_repaint(&mut self) {
        if let Some(ctx) = self.ctx() {
            ctx.request_repaint();
        }
    }
}
