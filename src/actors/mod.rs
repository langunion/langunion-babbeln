#![allow(unused_imports)]
pub mod app_actor;
pub mod blocked_users_actor;
pub mod chat_partners_actor;
pub mod friendrequests_actor;
pub mod friends_actor;
pub mod http_actor;
pub mod messages_actor;
pub mod posts_actor;
pub mod socket_actor;
pub mod user_actor;
pub mod webrtc_actor;

#[cfg(not(target_arch = "wasm32"))]
pub mod socket_actor_native;
#[cfg(target_arch = "wasm32")]
pub mod socket_actor_wasm;

#[cfg(not(target_arch = "wasm32"))]
pub mod webrtc_actor_native;
#[cfg(target_arch = "wasm32")]
pub mod webrtc_actor_wasm;

pub use app_actor::{use_app_handle, use_reset_handle, AppActor, AppHandle, ResetHandle, Window};
pub use blocked_users_actor::{use_blocked_users_handle, BlockedUsersActor, BlockedUsersHandle};
pub use chat_partners_actor::{use_chat_partners_handle, ChatPartnersActor, ChatPartnersHandle};
pub use friendrequests_actor::{
    use_friendrequests_handle, FriendrequestsActor, FriendrequestsHandle,
};
pub use friends_actor::{use_friends_handle, FriendsActor, FriendsHandle};
pub use http_actor::{use_http_handle, HttpActor, HttpError, HttpHandle};
pub use messages_actor::{use_messages_handle, MessagesActor, MessagesHandle};
pub use posts_actor::{use_posts_handle, PostsActor, PostsHandle};
pub use socket_actor::{use_socket_handle, SocketHandle};
pub use user_actor::{use_user_handle, UserActor, UserHandle};
pub use webrtc_actor::{use_webrtc_handle, WebrtcHandle};

#[cfg(not(target_arch = "wasm32"))]
pub use socket_actor_native::SocketActor;
#[cfg(target_arch = "wasm32")]
pub use socket_actor_wasm::SocketActor;

#[cfg(not(target_arch = "wasm32"))]
pub use webrtc_actor_native::WebrtcActor;
#[cfg(target_arch = "wasm32")]
pub use webrtc_actor_wasm::WebrtcActor;
