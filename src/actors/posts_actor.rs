use crate::actors::{use_blocked_users_handle, use_http_handle, HttpError, HttpHandle};
use crate::core::models::{Post, User};
use crate::core::responses::GetPostsResponse;
use crate::core::task::spawn;
use log::{debug, error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static POSTS_HANDLE: LazyLock<PostsHandle> = LazyLock::new(|| {
    debug!("Created POSTS_HANDLE");
    let (posts_actor, posts_handle) = PostsActor::new();
    PostsActor::run(posts_actor);
    posts_handle
});

pub fn use_posts_handle() -> PostsHandle {
    POSTS_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetPosts(Vec<Post>),
}

pub struct PostsActor {
    pub posts: watch::Sender<Vec<Post>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl PostsActor {
    pub fn new() -> (Self, PostsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_posts, watcher_posts) = watch::channel::<Vec<Post>>(Vec::new());

        let posts_actor = Self {
            posts: update_posts,
            receiver,
        };

        let posts_handle = PostsHandle::new(watcher_posts, sender);

        (posts_actor, posts_handle)
    }

    pub fn run(posts_actor: Self) {
        debug!("Started posts actor event loop");
        spawn(posts_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    PostsActor::fetch();
                }
                ActorMessage::SetPosts(posts) => {
                    debug!("ActorMessage::SetPots");
                    let _ = self.posts.send(posts);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetPostsResponse>(
                Method::GET,
                "/posts/",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_posts_handle().set_posts(response.result);
            }
        });
    }
}

#[derive(Clone)]
pub struct PostsHandle {
    pub posts: watch::Receiver<Vec<Post>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl PostsHandle {
    fn new(posts: watch::Receiver<Vec<Post>>, sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { posts, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_posts(Vec::new());
    }

    pub fn posts(&mut self) -> Vec<Post> {
        self.posts.borrow_and_update().clone()
    }

    pub fn posts_filtered_blocked(&mut self) -> Vec<Post> {
        let posts: Vec<Post> = self.posts();
        let blocked_user_ids: Vec<usize> = use_blocked_users_handle()
            .blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let filtered_posts: Vec<Post> = posts
            .into_iter()
            .filter(|post: &Post| {
                for user_id in &blocked_user_ids {
                    if post.author.contains(&format!("/users/{user_id}")) {
                        return false;
                    }
                }
                true
            })
            .collect();
        filtered_posts
    }

    pub fn set_posts(&self, posts: Vec<Post>) {
        let _ = self.sender.send(ActorMessage::SetPosts(posts));
    }

    /// Add a post to the posts state
    pub fn add_post(&mut self, post: Post) {
        let mut posts: Vec<Post> = self.posts();
        if !posts.contains(&post) {
            posts.insert(0, post);
            self.set_posts(posts);
        }
    }

    /// Remove a post from the posts state
    pub fn remove_post(&mut self, post: Post) {
        let posts: Vec<Post> = self
            .posts()
            .into_iter()
            .filter(|p| p.id != post.id)
            .collect();
        self.set_posts(posts);
    }

    /// Update a post of the posts state
    pub fn update_post(&mut self, post: Post) {
        let mut posts: Vec<Post> = self.posts();
        if let Some(idx) = posts.iter().position(|p| p.id == post.id) {
            posts[idx] = post;
            self.set_posts(posts);
        }
    }
}
