use crate::actors::{use_app_handle, use_http_handle, use_user_handle, HttpError, HttpHandle};
use crate::core::models::{Message, User};
use crate::core::notification::play_notification_sound;
use crate::core::responses::GetChatResponse;
use crate::core::task::spawn;
use log::{debug, info, warn};
use reqwest::Method;
use std::collections::HashMap;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

use super::UserHandle;

pub static MESSAGES_HANDLE: LazyLock<MessagesHandle> = LazyLock::new(|| {
    debug!("Created MESSAGES_HANDLE");
    let (messages_actor, messages_handle) = MessagesActor::new();
    MessagesActor::run(messages_actor);
    messages_handle
});

pub fn use_messages_handle() -> MessagesHandle {
    MESSAGES_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    SetChats(HashMap<usize, Vec<Message>>),
}

pub struct MessagesActor {
    pub chats: watch::Sender<HashMap<usize, Vec<Message>>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl MessagesActor {
    pub fn new() -> (Self, MessagesHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_chats, watcher_chats) =
            watch::channel::<HashMap<usize, Vec<Message>>>(HashMap::new());

        let messages_actor = Self {
            chats: update_chats,
            receiver,
        };

        let messages_handle = MessagesHandle::new(watcher_chats, sender);

        (messages_actor, messages_handle)
    }

    pub fn run(messages_actor: Self) {
        debug!("Started messages actor event loop");
        spawn(messages_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetChats(chats) => {
                    debug!("ActorMessage::SetChats");
                    let _ = self.chats.send(chats);
                }
            }
        }
    }
}

#[derive(Clone)]
pub struct MessagesHandle {
    pub chats: watch::Receiver<HashMap<usize, Vec<Message>>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl MessagesHandle {
    fn new(
        chats: watch::Receiver<HashMap<usize, Vec<Message>>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self { chats, sender }
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_chats(HashMap::new());
    }

    pub fn chats(&mut self) -> HashMap<usize, Vec<Message>> {
        self.chats.borrow_and_update().clone()
    }

    pub fn chat(&mut self, user_id: usize) -> Vec<Message> {
        let chats: HashMap<usize, Vec<Message>> = self.chats();
        match chats.get(&user_id) {
            Some(chats) => chats.clone(),
            None => Vec::new(),
        }
    }

    pub fn set_chats(&self, chats: HashMap<usize, Vec<Message>>) {
        let _ = self.sender.send(ActorMessage::SetChats(chats));
    }

    pub fn fetch_chat(&self, user_id: usize) {
        spawn(async move {
            async fn fetch_chat(user_id: usize, page: usize) -> Result<GetChatResponse, HttpError> {
                let receiver = use_http_handle().do_authenticated_request(
                    Method::GET,
                    format!("/messages/{user_id}/chat/?page_size=50&page={page}"),
                    None,
                );

                receiver.await.unwrap()
            }

            let mut messages_handle = use_messages_handle();
            let mut chats: HashMap<usize, Vec<Message>> = messages_handle.chats();

            let mut chat: Vec<Message> = Vec::new();
            let mut page: usize = 1;

            while let Ok(mut chat_response) = fetch_chat(user_id, page).await {
                page += 1;

                chat.append(&mut chat_response.result);

                if let None = chat_response.next {
                    break;
                }
            }

            chats.insert(user_id, chat);
            messages_handle.set_chats(chats);

            use_app_handle().request_repaint();
        });
    }

    /// Add a message to the messages state
    pub fn add_message(&mut self, message: Message) {
        let user_id: usize = use_user_handle().me().id;
        let pattern = format!("/users/{user_id}");
        let chat_user_id: Option<usize> = {
            if message.sender.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.receiver)
            } else if message.receiver.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.sender)
            } else {
                warn!("Should never happen");
                None
            }
        };
        let chat_user_id: usize = chat_user_id.unwrap();
        let mut chats = self.chats();
        let mut chat: Vec<Message> = self.chat(chat_user_id);
        if !chat.contains(&message) {
            chat.insert(0, message);
            chats.insert(chat_user_id, chat);
            self.set_chats(chats);
            play_notification_sound();
        }
    }

    /// Remove a message from the messages state
    pub fn remove_message(&mut self, message: Message) {
        let user_id: usize = use_user_handle().me().id;
        let pattern = format!("/users/{user_id}");
        let chat_user_id: Option<usize> = {
            if message.sender.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.receiver)
            } else if message.receiver.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.sender)
            } else {
                warn!("Should never happen");
                None
            }
        };
        let chat_user_id: usize = chat_user_id.unwrap();
        let mut chats = self.chats();
        let chat: Vec<Message> = self
            .chat(chat_user_id)
            .into_iter()
            .filter(|msg| msg.id == message.id)
            .collect();
        chats.insert(chat_user_id, chat);
        self.set_chats(chats);
    }

    /// Update a message of the messages state
    pub fn update_message(&mut self, message: Message) {
        let user_id: usize = use_user_handle().me().id;
        let pattern = format!("/users/{user_id}");
        let chat_user_id: Option<usize> = {
            if message.sender.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.receiver)
            } else if message.receiver.contains(&pattern) {
                UserHandle::get_user_id_by_url(&message.sender)
            } else {
                warn!("Should never happen");
                None
            }
        };
        let chat_user_id: usize = chat_user_id.unwrap();
        let mut chats = self.chats();
        let mut chat: Vec<Message> = self.chat(chat_user_id);
        if let Some(idx) = chat.iter().position(|msg| msg.id == message.id) {
            chat[idx] = message;
            chats.insert(chat_user_id, chat);
            self.set_chats(chats);
        }
    }

    /// Remove a whole chat from the messages state
    pub fn remove_chat(&mut self, user: User) {
        let mut chats = self.chats();
        chats.remove(&user.id);
        self.set_chats(chats);
    }
}
