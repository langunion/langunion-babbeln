use crate::actors::{use_http_handle, use_user_handle, HttpError, HttpHandle, UserHandle};
use crate::core::models::User;
use crate::core::responses::GetChatPartnersResponse;
use crate::core::task::spawn;
use log::{debug, error, info, warn};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static CHAT_PARTNERS_HANDLE: LazyLock<ChatPartnersHandle> = LazyLock::new(|| {
    debug!("Created CHAT_PARTNERS_HANDLE");
    let (chat_partners_actor, chat_partners_handle) = ChatPartnersActor::new();
    ChatPartnersActor::run(chat_partners_actor);
    chat_partners_handle
});

pub fn use_chat_partners_handle() -> ChatPartnersHandle {
    CHAT_PARTNERS_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetChatPartners(Vec<User>),
}

pub struct ChatPartnersActor {
    pub chat_partners: watch::Sender<Vec<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl ChatPartnersActor {
    pub fn new() -> (Self, ChatPartnersHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_chat_partners, watcher_chat_partners) = watch::channel::<Vec<User>>(Vec::new());

        let chat_partners_actor = Self {
            chat_partners: update_chat_partners,
            receiver,
        };

        let chat_partners_handle = ChatPartnersHandle::new(watcher_chat_partners, sender);

        (chat_partners_actor, chat_partners_handle)
    }

    pub fn run(chat_partners_actor: Self) {
        debug!("Started chat partners actor event loop");
        spawn(chat_partners_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    ChatPartnersActor::fetch();
                }
                ActorMessage::SetChatPartners(chat_partners) => {
                    debug!("ActorMessage::SetChatPartners");
                    let _ = self.chat_partners.send(chat_partners);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetChatPartnersResponse>(
                Method::GET,
                "/users/chat-partners/",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_chat_partners_handle().set_chat_partners(response.result);
            }
        });
    }
}

#[derive(Clone)]
pub struct ChatPartnersHandle {
    pub chat_partners: watch::Receiver<Vec<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl ChatPartnersHandle {
    fn new(
        chat_partners: watch::Receiver<Vec<User>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            chat_partners,
            sender,
        }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_chat_partners(Vec::new());
    }

    pub fn get_chat_partners(&mut self) -> Vec<User> {
        self.chat_partners.borrow_and_update().clone()
    }

    pub fn set_chat_partners(&self, chat_partners: Vec<User>) {
        let _ = self
            .sender
            .send(ActorMessage::SetChatPartners(chat_partners));
    }

    pub fn set_latest_chat_partner(&mut self, latest_chat_partner_id: usize) {
        let is_memo: bool = latest_chat_partner_id == use_user_handle().me().id;

        if is_memo {
            return;
        }

        let latest_chat_partner: Option<User> = UserHandle::get_user_by_id(latest_chat_partner_id);

        if let Some(latest_chat_partner) = latest_chat_partner {
            let mut chat_partners: Vec<User> = self
                .get_chat_partners()
                .into_iter()
                .filter(|chat_partner: &User| chat_partner.id != latest_chat_partner.id)
                .collect();
            chat_partners.insert(0, latest_chat_partner);
            self.set_chat_partners(chat_partners);
        } else {
            warn!("Set latest chat partner: no user found")
        }
    }
}
