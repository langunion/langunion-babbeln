use crate::actors::{use_http_handle, HttpError, HttpHandle};
use crate::core::models::{Friendrequest, User};
use crate::core::responses::GetFriendrequestsResponse;
use crate::core::task::spawn;
use log::{debug, error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static FRIENDREQUESTS_HANDLE: LazyLock<FriendrequestsHandle> = LazyLock::new(|| {
    debug!("Created FRIENDREQUESTS_HANDLE");
    let (friendrequests_actor, friendrequests_handle) = FriendrequestsActor::new();
    FriendrequestsActor::run(friendrequests_actor);
    friendrequests_handle
});

pub fn use_friendrequests_handle() -> FriendrequestsHandle {
    FRIENDREQUESTS_HANDLE.clone()
}

#[derive(Debug)]
enum ActorMessage {
    Fetch,
    SetFriendrequests(Vec<Friendrequest>),
}

pub struct FriendrequestsActor {
    pub friendrequests: watch::Sender<Vec<Friendrequest>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl FriendrequestsActor {
    pub fn new() -> (Self, FriendrequestsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_friendrequests, watcher_friendrequests) =
            watch::channel::<Vec<Friendrequest>>(Vec::new());

        let friendrequests_actor = Self {
            friendrequests: update_friendrequests,
            receiver,
        };

        let friendrequests_handle = FriendrequestsHandle::new(watcher_friendrequests, sender);

        (friendrequests_actor, friendrequests_handle)
    }

    pub fn run(friendrequests_actor: Self) {
        debug!("Started friendrequests actor event loop");
        spawn(friendrequests_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    debug!("ActorMessage::Fetch");
                    FriendrequestsActor::fetch();
                }
                ActorMessage::SetFriendrequests(friendrequests) => {
                    debug!("ActorMessage::SetFriendrequests");
                    let _ = self.friendrequests.send(friendrequests);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let receiver = use_http_handle().do_authenticated_request::<GetFriendrequestsResponse>(
                Method::GET,
                "/friendrequests/",
                None,
            );

            if let Ok(response) = receiver.await.unwrap() {
                use_friendrequests_handle().set_friendrequests(response.result);
            }
        });
    }
}

#[derive(Clone)]
pub struct FriendrequestsHandle {
    pub friendrequests: watch::Receiver<Vec<Friendrequest>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl FriendrequestsHandle {
    fn new(
        friendrequests: watch::Receiver<Vec<Friendrequest>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            friendrequests,
            sender,
        }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        debug!("Resetted state");
        self.set_friendrequests(Vec::new());
    }

    pub fn friendrequests(&mut self) -> Vec<Friendrequest> {
        self.friendrequests.borrow_and_update().clone()
    }

    pub fn set_friendrequests(&self, friendrequests: Vec<Friendrequest>) {
        let _ = self
            .sender
            .send(ActorMessage::SetFriendrequests(friendrequests));
    }

    /// Add a friendrequest to the friendrequests state
    pub fn add_friendrequest(&mut self, friendrequest: Friendrequest) {
        let mut friendrequests: Vec<Friendrequest> = self.friendrequests();
        if !friendrequests.contains(&friendrequest) {
            friendrequests.insert(0, friendrequest);
            self.set_friendrequests(friendrequests);
        }
    }

    /// Remove a friendrequest from the friendrequests state
    pub fn remove_friendrequest(&mut self, friendrequest: Friendrequest) {
        let friendrequests: Vec<Friendrequest> = self
            .friendrequests()
            .into_iter()
            .filter(|f| f.id != friendrequest.id)
            .collect();
        self.set_friendrequests(friendrequests);
    }

    /// Remove a user from the friendrequests state
    pub fn remove_friendrequest_with_user(&mut self, user: User) {
        let friendrequests: Vec<Friendrequest> = self
            .friendrequests()
            .into_iter()
            .filter(|f| {
                let pattern = format!("/users/{}", user.id);
                f.to_user.contains(&pattern) || f.from_user.contains(&pattern)
            })
            .collect();
        self.set_friendrequests(friendrequests);
    }
}
