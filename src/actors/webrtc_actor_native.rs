use crate::actors::webrtc_actor::{use_webrtc_handle, ActorMessage, WebrtcHandle};
use crate::core::microphone::Microphone;
use crate::core::task::spawn;
use crate::core::webcam::{Webcam, WebcamHandle};
use bytes::Bytes;
use log::{debug, error, info};
use std::sync::Arc;
use std::thread::JoinHandle;
use std::time::Duration;
use tokio::sync::{mpsc, watch};
use webrtc::api::interceptor_registry::register_default_interceptors;
use webrtc::api::media_engine::{MediaEngine, MIME_TYPE_OPUS, MIME_TYPE_VP8};
use webrtc::api::APIBuilder;
use webrtc::ice_transport::ice_server::RTCIceServer;
use webrtc::interceptor::registry::Registry;
use webrtc::peer_connection::{configuration::RTCConfiguration, RTCPeerConnection};
use webrtc::rtp_transceiver::rtp_codec::{RTCRtpCodecCapability, RTPCodecType};
use webrtc::track::track_local::{track_local_static_sample::TrackLocalStaticSample, TrackLocal};
use webrtc::track::track_remote::TrackRemote;

pub struct WebrtcActor {
    peer_connection: Option<Arc<RTCPeerConnection>>,
    peer_connection_media_pipeline: Option<(WebcamHandle, Microphone)>,
    screenshare_peer_connection: Option<Arc<RTCPeerConnection>>,
    screenshare_peer_connection_media_pipeline: Option<JoinHandle<()>>,
    call_key: watch::Sender<Option<String>>,
    is_in_call: watch::Sender<bool>,
    is_webcam_on: watch::Sender<bool>,
    is_microphone_on: watch::Sender<bool>,
    is_screenshare_on: watch::Sender<bool>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl WebrtcActor {
    pub fn new() -> (Self, WebrtcHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_call_key, watcher_call_key) = watch::channel::<Option<String>>(None);
        let (update_is_in_call, watcher_is_in_call) = watch::channel::<bool>(false);
        let (update_is_webcam_on, watcher_is_webcam_on) = watch::channel::<bool>(false);
        let (update_is_microphone_on, watcher_is_microphone_on) = watch::channel::<bool>(false);
        let (update_is_screenshare_on, watcher_is_screenshare_on) = watch::channel::<bool>(false);

        let webrtc_actor = Self {
            peer_connection: None,
            peer_connection_media_pipeline: None,
            screenshare_peer_connection: None,
            screenshare_peer_connection_media_pipeline: None,
            call_key: update_call_key,
            is_in_call: update_is_in_call,
            is_webcam_on: update_is_webcam_on,
            is_microphone_on: update_is_microphone_on,
            is_screenshare_on: update_is_screenshare_on,
            receiver,
        };

        let webrtc_handle = WebrtcHandle::new(
            watcher_call_key,
            watcher_is_in_call,
            watcher_is_webcam_on,
            watcher_is_microphone_on,
            watcher_is_screenshare_on,
            sender,
        );

        (webrtc_actor, webrtc_handle)
    }

    pub fn run(webrtc_actor: Self) {
        debug!("Started WebrtcActor event loop");
        spawn(webrtc_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Reset => {
                    debug!("ActorMessage::Reset");
                    self.peer_connection = None;
                    if let Some(pipeline) = self.peer_connection_media_pipeline.take() {
                        pipeline.0.stop_recording();
                        pipeline.1.stop_recording();
                    }
                    self.screenshare_peer_connection = None;
                    if let Some(pipeline) = self.screenshare_peer_connection_media_pipeline.take() {
                        drop(pipeline);
                    }
                    let _ = self.call_key.send(None);
                    let _ = self.is_in_call.send(false);
                    let _ = self.is_webcam_on.send(false);
                    let _ = self.is_microphone_on.send(false);
                    let _ = self.is_screenshare_on.send(false);
                }
                ActorMessage::StartCall => {
                    debug!("ActorMessage::StartCall");
                    let (peer_connection, pipeline) = Self::new_peer_connection().await;
                    self.peer_connection = Some(peer_connection);
                    self.peer_connection_media_pipeline = Some(pipeline);
                }
                ActorMessage::SetCallKey(call_key) => {
                    debug!("ActorMessage::SetCallKey");
                    let _ = self.call_key.send(call_key);
                }
                ActorMessage::EnableWebcam => {
                    debug!("ActorMessage::EnableWebcam");
                    let _ = self.is_webcam_on.send(true);
                }
                ActorMessage::DisableWebcam => {
                    debug!("ActorMessage::DisableWebcam");
                    let _ = self.is_webcam_on.send(false);
                }
                ActorMessage::EnableMicrophone => {
                    debug!("ActorMessage::EnableMicrophone");
                    let _ = self.is_microphone_on.send(true);
                }
                ActorMessage::DisableMicrophone => {
                    debug!("ActorMessage::DisableMicrophone");
                    let _ = self.is_microphone_on.send(false);
                }
                ActorMessage::EnableScreenshare => {
                    debug!("ActorMessage::EnableScreenshare");
                    let _ = self.is_screenshare_on.send(true);
                }
                ActorMessage::DisableSceenshare => {
                    debug!("ActorMessage::DisableScreenshare");
                    let _ = self.is_screenshare_on.send(false);
                }
            }
        }
    }

    async fn new_peer_connection() -> (Arc<RTCPeerConnection>, (WebcamHandle, Microphone)) {
        let config = RTCConfiguration {
            ice_servers: vec![RTCIceServer {
                urls: vec!["stun:stun.l.google.com:19302".to_owned()],
                ..Default::default()
            }],
            ..Default::default()
        };
        let mut m = MediaEngine::default();
        m.register_default_codecs().unwrap();
        let mut registry = Registry::new();
        registry = register_default_interceptors(registry, &mut m).unwrap();
        let api = APIBuilder::new()
            .with_media_engine(m)
            .with_interceptor_registry(registry)
            .build();
        let peer_connection = Arc::new(api.new_peer_connection(config).await.unwrap());

        // Handle remote tracks
        peer_connection.on_track(Box::new(move |track: Arc<TrackRemote>, _, _| {
            spawn(async move {
                let _data: Bytes = track.read_rtp().await.unwrap().0.payload;
                match track.kind() {
                    RTPCodecType::Video => {
                        // Handle incoming video frames
                        // tokio::spawn(handle_video_stream(track.clone()));
                        todo!();
                    }
                    RTPCodecType::Audio => {
                        todo!();
                    }
                    _ => {}
                }
            });
            Box::pin(async {})
        }));

        let pipeline = Self::mount_media_streams(peer_connection.clone()).await;
        (peer_connection, pipeline)
    }

    async fn mount_media_streams(
        peer_connection: Arc<RTCPeerConnection>,
    ) -> (WebcamHandle, Microphone) {
        let video_track = Arc::new(TrackLocalStaticSample::new(
            RTCRtpCodecCapability {
                mime_type: MIME_TYPE_VP8.to_string(),
                ..Default::default()
            },
            "video".to_string(),
            "langunion-babbeln".to_string(),
        ));
        let audio_track = Arc::new(TrackLocalStaticSample::new(
            RTCRtpCodecCapability {
                mime_type: MIME_TYPE_OPUS.to_string(),
                ..Default::default()
            },
            "audio".to_string(),
            "langunion-babbeln".to_string(),
        ));

        let rtp_video_sender = peer_connection
            .add_track(Arc::clone(&video_track) as Arc<dyn TrackLocal + Send + Sync>)
            .await
            .unwrap();
        let rtp_audio_sender = peer_connection
            .add_track(Arc::clone(&audio_track) as Arc<dyn TrackLocal + Send + Sync>)
            .await
            .unwrap();
        // Read incoming RTCP packets
        // Before these packets are returned they are processed by interceptors. For things
        // like NACK this needs to be called.
        tokio::spawn(async move {
            let mut rtcp_buf = vec![0u8; 1500];
            while let Ok((_, _)) = rtp_video_sender.read(&mut rtcp_buf).await {}
            while let Ok((_, _)) = rtp_audio_sender.read(&mut rtcp_buf).await {}
        });

        let (webcam, webcam_handle) = Webcam::new();
        webcam.start_recording(video_track.clone());

        let microphone = Microphone::new(audio_track.clone());
        microphone.start_recording();

        debug!("Starting webcam/microphone capture...");
        (webcam_handle, microphone)
    }
}
