use crate::actors::{
    use_app_handle, use_http_handle, use_reset_handle, AppHandle, HttpHandle, ResetHandle, Window,
};
use crate::core::notification::use_toasts;
use crate::screens::{Loading, LoginRegister, Main};
use langunion_ui::colors;
use eframe::{CreationContext, Frame, Storage};
use egui::{vec2, Context, CursorIcon, Style, Theme};
use log::{debug, info};

pub struct App {
    reset_handle: &'static mut ResetHandle,
    app_handle: AppHandle,
    http_handle: HttpHandle,
    loading: Loading,
    login_register: LoginRegister,
    main: Main,
}

impl App {
    pub fn new(cc: &CreationContext<'_>) -> Self {
        // DEBUG
        // crate::actors::use_webrtc_handle().start_call();
        // crate::core::task::spawn(async move {
        //     crate::core::task::wait_for(std::time::Duration::from_secs(15)).await;
        //     crate::actors::use_webrtc_handle().stop_call();
        // });

        cc.egui_ctx
            .style_mut_of(Theme::Light, Self::use_light_theme);
        cc.egui_ctx.style_mut_of(Theme::Dark, Self::use_dark_theme);

        let storage: &dyn Storage = cc.storage.expect("No Storage Support");

        if let Some(access_token) = storage.get_string("access_token") {
            if !access_token.is_empty() {
                info!("Found 'access_token' in Storage");
                futures::executor::block_on(async move {
                    use_http_handle()
                        .set_access_token_async(Some(access_token))
                        .await;
                });
            } else {
                info!("No 'access_token' in Storage found");
            }
        }

        if let Some(refresh_token) = storage.get_string("refresh_token") {
            if !refresh_token.is_empty() {
                info!("Found 'refresh_token' in Storage");
                futures::executor::block_on(async move {
                    use_http_handle()
                        .set_refresh_token_async(Some(refresh_token))
                        .await;
                });
            } else {
                info!("No 'refresh_token' in Storage found");
            }
        }

        Self {
            reset_handle: use_reset_handle(),
            app_handle: use_app_handle(),
            http_handle: use_http_handle(),
            loading: Loading::new(),
            login_register: LoginRegister::new(),
            main: Main::new(),
        }
    }

    #[cfg(not(target_arch = "wasm32"))]
    pub async fn start_native_app() {
        env_logger::Builder::from_env(env_logger::Env::default().filter_or("LOG_LEVEL", "debug"))
            .init();
        let native_options = eframe::NativeOptions {
            // TODO schau dir die Options an
            // initial_window_size: Some([400.0, 300.0].into()),
            // min_window_size: Some([300.0, 220.0].into()),
            ..Default::default()
        };
        eframe::run_native(
            "babbeln",
            native_options,
            Box::new(|cc| {
                egui_extras::install_image_loaders(&cc.egui_ctx);
                cc.egui_ctx.forget_all_images();
                Ok(Box::new(App::new(cc)))
            }),
        )
        .unwrap();
    }

    #[cfg(target_arch = "wasm32")]
    pub async fn start_wasm_app() {
        use eframe::wasm_bindgen::JsCast;
        use log::error;
        eframe::WebLogger::init(log::LevelFilter::Debug).unwrap();
        let web_options = eframe::WebOptions::default(); // TODO auch hier was ist alles möglich
        wasm_bindgen_futures::spawn_local(async {
            let document = web_sys::window()
                .expect("No window")
                .document()
                .expect("No document");

            let app = document
                .get_element_by_id("app")
                .expect("Failed to find #app element")
                .dyn_into::<web_sys::HtmlCanvasElement>()
                .expect("#app element was not a HtmlCanvasElement");

            let start_result = eframe::WebRunner::new()
                .start(app, web_options, Box::new(|cc| Ok(Box::new(App::new(cc)))))
                .await;

            let loading_screen = document
                .get_element_by_id("loading_screen")
                .expect("Missing #loading_screen element in index.html");
            match start_result {
                Ok(_) => {
                    loading_screen.remove();
                }
                Err(e) => {
                    // TODO crash site with support ticket system
                    loading_screen.set_inner_html(
                        "<p> The app has crashed. See the developer console for details. </p>",
                    );
                    error!("Failed to start babbeln: {e:?}");
                }
            }
        });
    }

    pub fn set_global_theme(style: &mut Style) {
        style.visuals.interact_cursor = Some(CursorIcon::PointingHand);
        style.spacing.item_spacing = vec2(8., 8.);
        style.visuals.widgets.active.expansion = 0.0;
        style.visuals.widgets.inactive.expansion = 0.0;
        style.visuals.widgets.noninteractive.expansion = 0.0;
        style.visuals.widgets.hovered.expansion = 0.0;
        style.visuals.widgets.open.expansion = 0.0;
    }

    pub fn use_light_theme(style: &mut Style) {
        Self::set_global_theme(style);
        style.visuals.panel_fill = colors::WHITE;
        style.visuals.extreme_bg_color = *colors::STONE_50;
    }

    pub fn use_dark_theme(style: &mut Style) {
        Self::set_global_theme(style);
        style.visuals.panel_fill = *colors::ZINC_900;
        style.visuals.extreme_bg_color = *colors::ZINC_950;
    }
}

impl eframe::App for App {
    fn save(&mut self, storage: &mut dyn Storage) {
        let access = self.http_handle.access_token().unwrap_or(String::new());
        let refresh = self.http_handle.refresh_token().unwrap_or(String::new());
        storage.set_string("access_token", access);
        storage.set_string("refresh_token", refresh);
        debug!("Saved auth tokens inside storage");
        storage.flush();
    }

    fn update(&mut self, ctx: &Context, _: &mut Frame) {
        self.app_handle.set_ctx(ctx.clone());
        use_toasts().show(ctx);

        if let Ok(_) = self.reset_handle.receiver.try_recv() {
            self.loading = Loading::new();
            self.login_register = LoginRegister::new();
            self.main = Main::new();
            debug!("Recreated GUI");
        }

        match self.app_handle.window() {
            Window::Loading => self.loading.render(ctx),
            Window::Login => self.login_register.render(ctx),
            Window::Main => self.main.render(ctx),
        };
    }
}
