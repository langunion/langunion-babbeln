use crate::actors::{use_webrtc_handle, WebrtcHandle};
use egui::{ColorImage, Context as UiContext, TextureHandle, Ui};

pub enum Kind {
    LocalWebcam,
    RemoteWebcam,
    LocalScreenshare,
    RemoteScreenshare,
}

impl Kind {
    fn egui_id(&self) -> &str {
        match self {
            Self::LocalWebcam => "videostream_local_webcam",
            Self::RemoteWebcam => "videostream_remote_webcam",
            Self::LocalScreenshare => "videostream_local_screenshare",
            Self::RemoteScreenshare => "videostream_remote_screenshare",
        }
    }
}

pub struct VideoStream {
    kind: Kind,
    image: Option<TextureHandle>,
    webrtc_handle: WebrtcHandle,
}

impl VideoStream {
    pub fn new(kind: Kind) -> Self {
        Self {
            kind,
            image: None,
            webrtc_handle: use_webrtc_handle(),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &UiContext) {
        self.updating_image(ctx);
        if let Some(image) = &self.image {
            ui.image(image);
        }
        ctx.request_repaint(); // Never stop re-rendering the video
    }

    fn updating_image(&mut self, ctx: &UiContext) {
        let frame = match self.kind {
            Kind::LocalWebcam => {
                // self.webrtc_handle.local_stream.video.1.try_recv().ok()
                self.webrtc_handle
                    .media_stream_channel(0)
                    .video
                    .1
                    .try_recv()
                    .ok()
            }
            _ => panic!("not implemented remove kind prop and replace it with user_id"),
        };

        if let Some(frame) = frame {
            let image = ColorImage::from_rgba_unmultiplied(
                [frame.width() as usize, frame.height() as usize],
                &frame.into_vec(),
            );
            let image = ctx.load_texture(self.kind.egui_id(), image, Default::default());
            self.image = Some(image);
        };
    }
}
