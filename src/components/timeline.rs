use crate::components::{CreatePost, Posts};
use egui::{Context, Ui};
use std::cell::RefCell;
use std::rc::Rc;

pub struct Timeline {
    create_post: CreatePost,
    posts: Posts,
    show_create_post: Rc<RefCell<bool>>,
}

impl Timeline {
    pub fn new() -> Self {
        let show_create_post: Rc<RefCell<bool>> = Rc::new(RefCell::new(false));
        Self {
            create_post: CreatePost::new(Rc::clone(&show_create_post)),
            posts: Posts::new(),
            show_create_post: Rc::clone(&show_create_post),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        egui::TopBottomPanel::top("timeline_top_panel").show_inside(ui, |ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label("Timeline");

                if ui.button("Create post").clicked() {
                    *self.show_create_post.borrow_mut() = true;
                }
            });
        });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            if *self.show_create_post.borrow() {
                self.create_post.render(ui, ctx);
            } else {
                self.posts.render(ui, ctx);
            }
        });
    }
}
