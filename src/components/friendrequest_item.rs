use crate::actors::{
    use_friendrequests_handle, use_friends_handle, use_http_handle, use_user_handle,
    FriendrequestsHandle, FriendsHandle, HttpError, HttpHandle, UserHandle,
};
use crate::core::models::{Friendrequest, User};
use crate::core::responses::{
    AcceptFriendrequestResponse, DeleteFriendrequestResponse, GetUserResponse,
};
use langunion_ui::{colors, Button, Container, ContainerComponent};
use egui::{vec2, Context, RichText, Stroke, Ui};
use reqwest::Method;
use tokio::sync::oneshot;

pub struct FriendrequestItem {
    friendrequests_handle: FriendrequestsHandle,
    friends_handle: FriendsHandle,
    user_handle: UserHandle,
    friendrequest: Friendrequest,
    friendrequest_user: Option<User>,
    get_user_response: Option<oneshot::Receiver<Result<GetUserResponse, HttpError>>>,
    accept_response: Option<oneshot::Receiver<Result<AcceptFriendrequestResponse, HttpError>>>,
    decline_response: Option<oneshot::Receiver<Result<DeleteFriendrequestResponse, HttpError>>>,
    is_sender_me: bool,
}

impl FriendrequestItem {
    pub fn new(friendrequest: Friendrequest) -> Self {
        let mut user_handle = use_user_handle();
        let me: User = user_handle.me();
        let is_sender_me = friendrequest
            .from_user
            .contains(&format!("/users/{}", me.id));
        let get_user_route = match is_sender_me {
            true => &friendrequest.to_user,
            false => &friendrequest.from_user,
        };
        let receiver = use_http_handle().do_authenticated_request::<GetUserResponse>(
            Method::GET,
            get_user_route,
            None,
        );

        Self {
            friendrequests_handle: use_friendrequests_handle(),
            friends_handle: use_friends_handle(),
            user_handle,
            friendrequest,
            friendrequest_user: None,
            get_user_response: Some(receiver),
            accept_response: None,
            decline_response: None,
            is_sender_me,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_responses();

        let dark_mode = ui.style().visuals.dark_mode;

        match self.friendrequest_user.clone() {
            Some(user) => Container::new()
                .padding(10.)
                .full_width(ui)
                .background(match dark_mode {
                    false => *colors::STONE_50,
                    true => *colors::ZINC_800,
                })
                .border(match dark_mode {
                    false => Stroke::new(1., *colors::STONE_100),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui| {
                    ui.horizontal(|ui: &mut Ui| {
                        Container::new()
                            .radius(5.)
                            .border(match dark_mode {
                                false => (3., *colors::GRAY_300).into(),
                                true => (3., *colors::GRAY_700).into(),
                            })
                            .background(match dark_mode {
                                false => *colors::GRAY_200,
                                true => *colors::GRAY_600,
                            })
                            .show(ui, |ui| {
                                ui.add(
                                    egui::Image::from_uri(&user.avatar)
                                        .fit_to_exact_size(vec2(40., 40.))
                                        .rounding(5.),
                                );
                            });

                        ui.label(
                            RichText::new(&user.username)
                                .color(match dark_mode {
                                    false => colors::WHITE,
                                    true => colors::WHITE,
                                })
                                .size(14.),
                        );

                        ui.label("Still pending ...");

                        match self.is_sender_me {
                            true => {
                                if Button::new().label("Cancel").normal(ui).add(ui).clicked() {
                                    self.decline();
                                }
                            }
                            false => {
                                if Button::new().label("Accept").normal(ui).add(ui).clicked() {
                                    self.accept();
                                }

                                if Button::new().label("Decline").normal(ui).add(ui).clicked() {
                                    self.decline();
                                }
                            }
                        }
                    });
                }),
            None => Container::new()
                .padding(10.)
                .full_width(ui)
                .height(50.)
                .background(match dark_mode {
                    false => *colors::STONE_50,
                    true => *colors::ZINC_800,
                })
                .border(match dark_mode {
                    false => Stroke::new(1., *colors::STONE_100),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui| {}),
        };
    }

    fn accept(&mut self) {
        let receiver = use_http_handle().do_authenticated_request::<AcceptFriendrequestResponse>(
            Method::PATCH,
            format!("/friendrequests/{}/accept/", self.friendrequest.id),
            None,
        );
        self.accept_response = Some(receiver);
    }

    fn decline(&mut self) {
        let receiver = use_http_handle().do_authenticated_request::<DeleteFriendrequestResponse>(
            Method::DELETE,
            format!("/friendrequests/{}/", self.friendrequest.id),
            None,
        );
        self.decline_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.get_user_response,
            |data: GetUserResponse| {
                self.friendrequest_user = Some(data.result);
            },
            |_error| {},
        );

        HttpHandle::handle_response_data(
            &mut self.accept_response,
            |_data: AcceptFriendrequestResponse| {
                self.friendrequests_handle.fetch();
                self.friends_handle.fetch();
            },
            |_error| {},
        );

        HttpHandle::handle_response_data(
            &mut self.decline_response,
            |_data: DeleteFriendrequestResponse| {
                self.friendrequests_handle.fetch();
            },
            |_error| {},
        );
    }
}
