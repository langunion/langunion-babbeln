use crate::actors::UserHandle;
use crate::core::models::Post;
use egui::{Context, Ui};

pub struct PostItem {
    post: Post,
    author: String,
}

impl PostItem {
    pub fn new(post: Post) -> Self {
        let author: String = UserHandle::display_username_by_url(&post.author);

        Self { post, author }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.author);
                ui.label(format!(
                    "{}",
                    &self.post.created_at.format("%d/%m/%Y %H:%M")
                ));
            });

            ui.label(&format!("images: {:?}", &self.post.images));
            ui.label(&self.post.content);

            ui.horizontal(|ui: &mut Ui| {
                ui.label(&format!("likes: {:?}", &self.post.likes));
                ui.label(&format!("comments: {}", &self.post.comments.len()));
            });
        });
    }
}
