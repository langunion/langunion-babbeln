use crate::actors::{
    use_app_handle, use_http_handle, use_socket_handle, use_user_handle, HttpError, HttpHandle,
    Window,
};
use crate::core::responses::LoginResponse;
use langunion_ui::{colors, Alert, Button, Container, ContainerComponent, Heading, Input, Label, TextAlign};
use egui::{Align, Context, Layout, Stroke, Ui};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct Login {
    http_handle: HttpHandle,
    email: String,
    password: String,
    login_response: Option<oneshot::Receiver<Result<LoginResponse, HttpError>>>,
    login_error: Option<String>,
}

impl Login {
    pub fn new() -> Self {
        Self {
            http_handle: use_http_handle(),
            email: String::new(),
            password: String::new(),
            login_response: None,
            login_error: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_login_response();

        ui.with_layout(Layout::top_down(Align::Center), |ui| {
            Container::new()
                .max_width(400.)
                .padding(10.)
                .border(match ui.style().visuals.dark_mode {
                    false => Stroke::new(1., *colors::ZINC_300),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui| {
                    ui.with_layout(Layout::top_down(Align::LEFT), |ui| {
                        Heading::new("Login").size(5).text_align_center().add(ui);
                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "E-mail").add(ui);
                        });
                        Input::new(&mut self.email)
                            .normal(&ui)
                            .placeholder("email...")
                            .full_width()
                            .add(ui);

                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "Password").add(ui);
                        });
                        Input::new(&mut self.password)
                            .normal(&ui)
                            .placeholder("password...")
                            .password(true)
                            .full_width()
                            .add(ui);

                        if let Some(login_error) = &self.login_error {
                            Alert::new().text(login_error).error(ui);
                        }

                        ui.add_space(10.);

                        if Button::new()
                            .label("Login")
                            .primary(&ui)
                            .full_width(true)
                            .text_align(TextAlign::Center)
                            .add_enabled(self.login_response.is_none(), ui)
                            .clicked()
                        {
                            crate::core::notification::play_notification_sound();
                            self.login();
                        }
                    });
                });
        });
    }

    fn login(&mut self) {
        let email: String = self.email.clone();
        let password: String = self.password.clone();
        let receiver = use_http_handle().do_request(
            Method::POST,
            "/tokens/",
            Some(json!({
                "email": email,
                "password": password,
            })),
        );
        self.login_response = Some(receiver);
    }

    fn handle_login_response(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.login_response,
            |data: LoginResponse| {
                // self.login_error = None;
                self.email = String::new();
                self.password = String::new();
                self.http_handle.set_access_token(Some(data.access));
                self.http_handle.set_refresh_token(Some(data.refresh));
                crate::core::task::spawn(async {
                    HttpHandle::fetch_all();
                    let _ = use_user_handle().me_async().await;
                    use_socket_handle().connect();
                    use_app_handle().set_window(Window::Main);
                });
            },
            |error| {
                self.login_error = Some(error.message);
            },
        );
    }
}
