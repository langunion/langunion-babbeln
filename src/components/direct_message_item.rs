use crate::actors::{
    use_app_handle, use_blocked_users_handle, use_friends_handle, use_messages_handle, AppHandle,
    BlockedUsersHandle, FriendsHandle, HttpError, HttpHandle, MessagesHandle, UserHandle,
};
use crate::core::models::User;
use crate::core::responses::{BlockUserResponse, RemoveFriendResponse, UnblockUserResponse};
use crate::modals::ProfileModal;
use langunion_ui::{colors, Container, ContainerComponent, Style, Styles};
use egui::{vec2, Context, CursorIcon, Sense, Stroke, Ui};
use tokio::sync::oneshot;

pub struct DirectMessageItem {
    app_handle: AppHandle,
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    messages_handle: MessagesHandle,
    user: User,
    username: String,
    is_blocked: bool,
    is_friend: bool,
    block_response: Option<oneshot::Receiver<Result<BlockUserResponse, HttpError>>>,
    unblock_response: Option<oneshot::Receiver<Result<UnblockUserResponse, HttpError>>>,
    remove_response: Option<oneshot::Receiver<Result<RemoveFriendResponse, HttpError>>>,
    profile_modal: ProfileModal,
}

impl DirectMessageItem {
    pub fn new(user: User) -> Self {
        let mut friends_handle = use_friends_handle();
        let mut blocked_users_handle = use_blocked_users_handle();

        let username: String = { UserHandle::display_username(&user) };
        let is_blocked: bool = blocked_users_handle.is_blocked(&user);
        let is_friend: bool = friends_handle.is_friend(&user);

        Self {
            app_handle: use_app_handle(),
            blocked_users_handle,
            friends_handle,
            messages_handle: use_messages_handle(),
            user: user.clone(),
            username,
            is_blocked,
            is_friend,
            block_response: None,
            unblock_response: None,
            remove_response: None,
            profile_modal: ProfileModal::new(user),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.profile_modal.render(ui, ctx);

        let is_active: bool = {
            let chat_partner_id: usize = self.app_handle.show_chat().unwrap_or(0);
            chat_partner_id == self.user.id
        };
        let dark_mode = ui.style().visuals.dark_mode;

        let item = Container::new()
            .sense(Sense::click())
            .padding(10.)
            .full_width(ui)
            .background(match dark_mode {
                false => match is_active {
                    true => *colors::SLATE_700, // light & active
                    false => *colors::STONE_50, // light & not active
                },
                true => match is_active {
                    true => *colors::INDIGO_600, // dark & active
                    false => *colors::ZINC_800,  // dark & not active
                },
            })
            .border(match dark_mode {
                false => match is_active {
                    true => Stroke::new(1., *colors::SLATE_800), // light & active
                    false => Stroke::new(1., *colors::STONE_300), // light & not active
                },
                true => match is_active {
                    true => Stroke::new(1., *colors::INDIGO_500), // dark & active
                    false => Stroke::new(1., *colors::ZINC_700),  // dark & not active
                },
            })
            .styles(match dark_mode {
                false => match is_active {
                    true => vec![Style::Color(colors::WHITE)], // light & active
                    false => vec![Style::Color(*colors::STONE_950)], // light & not active
                },
                true => match is_active {
                    true => vec![Style::Color(colors::WHITE)], // dark & active
                    false => vec![Style::Color(*colors::ZINC_300)], // dark & not active
                },
            })
            .show(ui, |ui| {
                ui.horizontal(|ui| {
                    Container::new()
                        .border(Stroke::new(2., *colors::EMERALD_300))
                        .show(ui, |ui| {
                            ui.add(
                                egui::Image::from_uri(&self.user.avatar)
                                    .fit_to_exact_size(vec2(30., 30.))
                                    .rounding(5.),
                            );
                        });
                    ui.add_space(5.);
                    ui.label(&self.username);
                });
            })
            .on_hover_cursor(CursorIcon::PointingHand);

        item.context_menu(|ui| {
            if ui.button("Show profile").clicked() {
                self.show_profile();
                ui.close_menu();
            }

            if self.is_blocked {
                if ui.button(format!("Unblock {}", &self.username)).clicked() {
                    self.unblock();
                    ui.close_menu();
                }
            } else {
                if ui.button(format!("Block {}", &self.username)).clicked() {
                    self.block();
                    ui.close_menu();
                }
            }

            if self.is_friend {
                if ui.button(format!("Remove {}", &self.username)).clicked() {
                    self.remove();
                    ui.close_menu();
                }
            }
        });

        if item.clicked() {
            self.app_handle.set_show_chat(self.user.id);
            self.messages_handle.fetch_chat(self.user.id);
        }
    }

    fn show_profile(&mut self) {
        self.profile_modal.open();
    }

    fn block(&mut self) {
        let receiver = BlockedUsersHandle::block_user(self.user.id);
        self.block_response = Some(receiver);
    }

    fn unblock(&mut self) {
        let receiver = BlockedUsersHandle::unblock_user(self.user.id);
        self.unblock_response = Some(receiver);
    }

    fn remove(&mut self) {
        let receiver = FriendsHandle::unfriend_user(self.user.id);
        self.remove_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.block_response,
            |_data: BlockUserResponse| {
                self.blocked_users_handle.fetch();
            },
            |_error| {},
        );

        HttpHandle::handle_response_data(
            &mut self.unblock_response,
            |_data: UnblockUserResponse| {
                self.blocked_users_handle.fetch();
            },
            |_error| {},
        );

        HttpHandle::handle_response_data(
            &mut self.remove_response,
            |_data: RemoveFriendResponse| {
                self.friends_handle.fetch();
            },
            |_error| {},
        );
    }
}
