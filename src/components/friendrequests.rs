use crate::actors::{use_app_handle, use_friendrequests_handle, FriendrequestsHandle};
use crate::components::FriendrequestItem;
use crate::core::models::Friendrequest;
use crate::core::task::{handle_oneshot_receiver, spawn_blocking};
use egui::{Context, Ui};
use log::debug;
use tokio::sync::oneshot;

pub struct Friendrequests {
    friendrequests_handle: FriendrequestsHandle,
    friendrequest_items: Vec<FriendrequestItem>,
    get_friendrequest_items: Option<oneshot::Receiver<Vec<FriendrequestItem>>>,
}

impl Friendrequests {
    pub fn new() -> Self {
        Self {
            friendrequests_handle: use_friendrequests_handle(),
            friendrequest_items: Vec::new(),
            get_friendrequest_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_friendrequest_items();

        if self.friendrequest_items.len() > 0 {
            egui::ScrollArea::vertical()
                .max_width(std::f32::INFINITY)
                .max_height(std::f32::INFINITY)
                .auto_shrink([false, false])
                .show_viewport(ui, |ui: &mut Ui, _| {
                    for friendrequest_item in &mut self.friendrequest_items {
                        friendrequest_item.render(ui, ctx);
                    }
                });
        } else {
            ui.label("no friendrequests ;(");
        }
    }

    fn handle_update_friendrequest_items(&mut self) {
        if self
            .friendrequests_handle
            .friendrequests
            .has_changed()
            .unwrap()
        {
            debug!("Friendrequests changed");
            self.build_friendrequest_items();
        }

        handle_oneshot_receiver(
            &mut self.get_friendrequest_items,
            |data: Vec<FriendrequestItem>| self.friendrequest_items = data,
        );
    }

    fn build_friendrequest_items(&mut self) {
        debug!("Rebuild friendrequest_items");
        let (sender, receiver) = oneshot::channel::<Vec<FriendrequestItem>>();
        let friendrequests = self.friendrequests_handle.friendrequests();

        spawn_blocking(|| {
            let friendrequest_items = friendrequests
                .into_iter()
                .map(|req: Friendrequest| FriendrequestItem::new(req))
                .collect();

            let _ = sender.send(friendrequest_items);
            use_app_handle().request_repaint();
        });

        self.get_friendrequest_items = Some(receiver);
    }
}
