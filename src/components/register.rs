use crate::actors::{use_http_handle, HttpError, HttpHandle};
use crate::core::responses::RegisterResponse;
use langunion_ui::{colors, Alert, Button, Container, ContainerComponent, Heading, Input, Label, TextAlign};
use egui::{Align, Context, Layout, Stroke, Ui};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct Register {
    email: String,
    username: String,
    password: String,
    second_password: String,
    register_response: Option<oneshot::Receiver<Result<RegisterResponse, HttpError>>>,
    register_error: Option<String>,
}

impl Register {
    pub fn new() -> Self {
        Self {
            email: String::new(),
            username: String::new(),
            password: String::new(),
            second_password: String::new(),
            register_response: None,
            register_error: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_register_response();

        ui.with_layout(Layout::top_down(Align::Center), |ui| {
            Container::new()
                .max_width(400.)
                .padding(10.)
                .border(match ui.style().visuals.dark_mode {
                    false => Stroke::new(1., *colors::ZINC_300),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui: &mut Ui| {
                    ui.with_layout(Layout::top_down(Align::LEFT), |ui| {
                        Heading::new("Create a new Account!")
                            .size(5)
                            .text_align_center()
                            .add(ui);
                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "E-mail").add(ui);
                        });
                        Input::new(&mut self.email)
                            .normal(&ui)
                            .placeholder("email...")
                            .full_width()
                            .add(ui);

                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "Username").add(ui);
                        });
                        Input::new(&mut self.username)
                            .normal(&ui)
                            .placeholder("username...")
                            .full_width()
                            .add(ui);

                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "Password").add(ui);
                        });
                        Input::new(&mut self.password)
                            .normal(&ui)
                            .placeholder("password...")
                            .password(true)
                            .full_width()
                            .add(ui);

                        Container::new().padding((6., 1., 2., 1.)).show(ui, |ui| {
                            Label::form(&ui, "Retype your password").add(ui);
                        });
                        Input::new(&mut self.second_password)
                            .normal(&ui)
                            .placeholder("retype password...")
                            .password(true)
                            .full_width()
                            .add(ui);

                        if let Some(register_error) = &self.register_error {
                            Alert::new().text(register_error).error(ui);
                        }

                        ui.add_space(10.);

                        if Button::new()
                            .label("Register")
                            .primary(&ui)
                            .full_width(true)
                            .text_align(TextAlign::Center)
                            .add_enabled(self.register_response.is_none(), ui)
                            .clicked()
                        {
                            crate::core::notification::play_notification_sound();
                            self.register();
                        }
                    });
                });
        });
    }

    fn register(&mut self) {
        let username: String = self.username.clone();
        let email: String = self.email.clone();
        let password: String = self.password.clone();
        let receiver = use_http_handle().do_request(
            Method::POST,
            "/users/",
            Some(json!({
                "username": username,
                "email": email,
                "password": password,
            })),
        );
        self.register_response = Some(receiver);
    }

    fn handle_register_response(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.register_response,
            |_data: RegisterResponse| {
                // TODO automatically log in the user
                // maybe add auth tokens to the response
                self.email = String::new();
                self.username = String::new();
                self.password = String::new();
                self.second_password = String::new();
            },
            |error| {
                self.register_error = Some(error.message);
            },
        );
    }
}
