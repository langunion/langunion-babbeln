use crate::actors::{use_http_handle, HttpError, HttpHandle};
use crate::components::AddFriendItem;
use crate::core::models::User;
use crate::core::responses::GetUsersResponse;
use langunion_ui::{colors, Button, ButtonImage, Input, Styles};
use egui::{vec2, Color32, Context, Frame, Ui};
use reqwest::Method;
use tokio::sync::oneshot;

pub struct AddFriend {
    query: String,
    users: Vec<AddFriendItem>,
    get_users_response: Option<oneshot::Receiver<Result<GetUsersResponse, HttpError>>>,
}

impl AddFriend {
    pub fn new() -> Self {
        Self {
            query: String::new(),
            users: Vec::new(),
            get_users_response: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();

        egui::TopBottomPanel::top("addfriend_top_panel")
            .frame(Frame::side_top_panel(ui.style()).fill(Color32::TRANSPARENT))
            .exact_height(50.)
            .show_separator_line(false)
            .resizable(false)
            .show_inside(ui, |ui| {
                egui::SidePanel::right("add_friend_top_panel_side_panel")
                    .frame(Frame::side_top_panel(ui.style()).fill(Color32::TRANSPARENT))
                    .exact_width(50.)
                    .show_separator_line(false)
                    .resizable(false)
                    .show_inside(ui, |ui| {
                        ui.horizontal_centered(|ui| {
                            if Button::new()
                                .image(ButtonImage {
                                    id: "bytes://search.svg".into(),
                                    bytes: include_bytes!("../../assets/icons/search.svg"),
                                    size: vec2(15., 15.),
                                })
                                .normal(ui)
                                .add(ui)
                                .clicked()
                            {
                                self.search_users();
                            }
                        });
                    });

                egui::CentralPanel::default()
                    .frame(Frame::central_panel(ui.style()).fill(Color32::TRANSPARENT))
                    .show_inside(ui, |ui| {
                    ui.horizontal_centered(|ui| {
                        Input::new(&mut self.query)
                            .normal(&ui)
                            .placeholder("Enter a username or email")
                            .full_width()
                            .add(ui);
                    });
                });
            });

        egui::CentralPanel::default()
            .frame(Frame::central_panel(ui.style()).fill(Color32::TRANSPARENT))
            .show_inside(ui, |ui| {
            egui::ScrollArea::vertical()
                .max_width(std::f32::INFINITY)
                .max_height(std::f32::INFINITY)
                .auto_shrink([false, false])
                .show_viewport(ui, |ui: &mut Ui, _| {
                    self.users.iter_mut().for_each(|user: &mut AddFriendItem| {
                        user.render(ui, ctx);
                    })
                });
        });
    }

    fn search_users(&mut self) {
        let query: String = self.query.clone();
        let receiver = use_http_handle().do_authenticated_request::<GetUsersResponse>(
            Method::GET,
            format!("/users/?query={query}"),
            None,
        );
        self.get_users_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.get_users_response,
            |data: GetUsersResponse| {
                self.users = data
                    .result
                    .into_iter()
                    .map(|user: User| AddFriendItem::new(user))
                    .collect()
            },
            |_error| {},
        );
    }
}
