use crate::actors::{use_friendrequests_handle, use_http_handle, HttpError, HttpHandle};
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::SendFriendrequestResponse;
use crate::modals::ProfileModal;
use egui::{Context, Ui};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct AddFriendItem {
    user: User,
    send_friendrequest_response:
        Option<oneshot::Receiver<Result<SendFriendrequestResponse, HttpError>>>,
    profile_modal: ProfileModal,
}

impl AddFriendItem {
    pub fn new(user: User) -> Self {
        Self {
            user: user.clone(),
            send_friendrequest_response: None,
            profile_modal: ProfileModal::new(user),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.profile_modal.render(ui, ctx);

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.user.username);

                if ui.button("show profile").clicked() {
                    self.show_profile();
                }

                if ui.button("add").clicked() {
                    self.send_friendrequest(self.user.id);
                }
            });
        });
    }

    fn show_profile(&mut self) {
        self.profile_modal.open();
    }

    fn send_friendrequest(&mut self, user_id: usize) {
        let receiver = use_http_handle().do_authenticated_request::<SendFriendrequestResponse>(
            Method::POST,
            "/friendrequests/",
            Some(json!({
                    "to_user": user_id,
            })),
        );

        self.send_friendrequest_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.send_friendrequest_response,
            |_data: SendFriendrequestResponse| {
                use_friendrequests_handle().fetch();
                create_toast(NotificationKind::Success, "Send friendrequest")
            },
            |_error| {},
        );
    }
}
