use crate::components::{AddFriend, BlockedUsers, Friendrequests, MyFriends};
use langunion_ui::{colors, Button, Container, ContainerComponent};
use egui::{vec2, Context, Stroke, Ui};

#[derive(PartialEq)]
enum Show {
    Friends,
    Friendrequests,
    BlockedUsers,
    AddFriend,
}

pub struct Friends {
    my_friends: MyFriends,
    friendrequests: Friendrequests,
    blocked_users: BlockedUsers,
    add_friend: AddFriend,
    show: Show,
}

impl Friends {
    pub fn new() -> Self {
        Self {
            my_friends: MyFriends::new(),
            friendrequests: Friendrequests::new(),
            blocked_users: BlockedUsers::new(),
            add_friend: AddFriend::new(),
            show: Show::Friends,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        egui::TopBottomPanel::top("friends_top_panel")
            .exact_height(50.)
            .resizable(false)
            .show_separator_line(false)
            .show_inside(ui, |ui: &mut Ui| {
                ui.horizontal_centered(|ui: &mut Ui| {
                    ui.add(
                        egui::Image::from_bytes(
                            "bytes://users(25x25).svg",
                            include_bytes!("../../assets/icons/users.svg"),
                        )
                        .tint(ui.style().visuals.text_color())
                        .fit_to_exact_size(vec2(25., 25.)),
                    );

                    ui.add_space(10.);

                    if Button::new()
                        .label("Friends")
                        .is_primary(self.show == Show::Friends, ui)
                        .add(ui)
                        .clicked()
                    {
                        self.show = Show::Friends;
                    }

                    if Button::new()
                        .label("Requests")
                        .is_primary(self.show == Show::Friendrequests, ui)
                        .add(ui)
                        .clicked()
                    {
                        self.show = Show::Friendrequests;
                    }

                    if Button::new()
                        .label("Blocked users")
                        .is_primary(self.show == Show::BlockedUsers, ui)
                        .add(ui)
                        .clicked()
                    {
                        self.show = Show::BlockedUsers;
                    }

                    if Button::new()
                        .label("Add friend")
                        .is_primary(self.show == Show::AddFriend, ui)
                        .add(ui)
                        .clicked()
                    {
                        self.show = Show::AddFriend;
                    }
                })
            });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            Container::new()
                .radius(10.)
                .padding((3., 10.))
                .full_width(ui)
                .full_height(ui)
                .background(match ui.style().visuals.dark_mode {
                    false => *colors::STONE_50,
                    true => *colors::ZINC_800,
                })
                .border(match ui.style().visuals.dark_mode {
                    false => Stroke::new(1., *colors::STONE_100),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui| {
                    match self.show {
                        Show::Friends => self.my_friends.render(ui, ctx),
                        Show::Friendrequests => self.friendrequests.render(ui, ctx),
                        Show::BlockedUsers => self.blocked_users.render(ui, ctx),
                        Show::AddFriend => self.add_friend.render(ui, ctx),
                    }
                });
        });
    }
}
