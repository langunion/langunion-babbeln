use crate::actors::use_user_handle;
use crate::core::models::{Message, User};
use langunion_ui::{colors, Container, ContainerComponent};
use chrono::{Datelike, Utc};
use egui::{vec2, Context, Label, RichText, Sense, Stroke, Ui};
use log::error;

pub struct MessageItem {
    message: Message,
    me: User,
    is_sender_me: bool,
}

impl MessageItem {
    pub fn new(message: Message) -> Self {
        let me: User = use_user_handle().me();
        let is_sender_me: bool = message.sender.contains(&format!("/users/{}", me.id));

        Self {
            message,
            me,
            is_sender_me,
        }
    }

    pub fn render(
        &mut self,
        ui: &mut Ui,
        _ctx: &Context,
        chat_partner: &User,
        chat_partner_username: &str,
    ) {
        let dark_mode = ui.style().visuals.dark_mode;

        ui.horizontal(|ui| {
            Container::new()
                .margin((5. + 2., 0.)) // margin + border - message.border
                .radius(5.)
                .border(match dark_mode {
                    false => (3., *colors::GRAY_300).into(),
                    true => (3., *colors::GRAY_700).into(),
                })
                .background(match dark_mode {
                    false => *colors::GRAY_200,
                    true => *colors::GRAY_600,
                })
                .show(ui, |ui| {
                    ui.add(
                        egui::Image::from_uri(&chat_partner.avatar)
                            .fit_to_exact_size(vec2(40., 40.))
                            .rounding(5.),
                    );
                });

            ui.vertical(|ui| {
                let message = Container::new()
                    .background(match dark_mode {
                        false => match self.is_sender_me {
                            true => *colors::STONE_100, // light mode & my message
                            false => *colors::BLUE_100, // light mode & other user message
                        },
                        true => match self.is_sender_me {
                            true => *colors::ZINC_800,  // dark mode & my message
                            false => *colors::BLUE_900, // dark mode & other user message
                        },
                    })
                    .border(match dark_mode {
                        false => match self.is_sender_me {
                            true => Stroke::new(1., *colors::STONE_200), // light mode & my message
                            false => Stroke::new(1., *colors::BLUE_200), // light mode & other user message
                        },
                        true => match self.is_sender_me {
                            true => Stroke::new(1., *colors::ZINC_700), // dark mode & my message
                            false => Stroke::new(1., *colors::BLUE_800), // dark mode & other user message
                        },
                    })
                    .radius(5.)
                    .padding(10.)
                    .margin((5., 0.))
                    .sense(Sense::click())
                    .show(ui, |ui| {
                        let username = match self.is_sender_me {
                            true => &self.me.username,
                            false => chat_partner_username,
                        };
                        let color = match dark_mode {
                            false => colors::BLACK,
                            true => colors::WHITE,
                        };
                        let message_date = {
                            let days = self.message.send_at.num_days_from_ce();
                            let now = Utc::now().num_days_from_ce();
                            match now - days {
                                0 => format!("Today {}", self.message.send_at.format("%H:%M")),
                                1 => format!("Yesterday {}", self.message.send_at.format("%H:%M")),
                                _ => format!("{}", self.message.send_at.format("%d.%m.%Y %H:%M")),
                            }
                        };

                        ui.horizontal(|ui| {
                            ui.label(RichText::new(username).color(color).strong());
                            ui.add_space(5.);
                            ui.label(RichText::new(message_date).color(*colors::GRAY_400));
                        });
                        ui.add(
                            Label::new(RichText::new(&self.message.content).color(color)).wrap(),
                        );
                    });

                message.context_menu(|ui| {
                    if self.is_sender_me {
                        if ui.button("copy").clicked() {
                            error!("todo copy clicked");
                            ui.close_menu();
                        }

                        if ui.button("edit").clicked() {
                            error!("todo edit clicked");
                            ui.close_menu();
                        }

                        if ui.button("delete").clicked() {
                            error!("todo delete clicked");
                            ui.close_menu();
                        }
                    } else {
                        if ui.button("copy").clicked() {
                            error!("todo copy clicked");
                            ui.close_menu();
                        }

                        if ui.button("delete").clicked() {
                            error!("todo delete clicked");
                            ui.close_menu();
                        }
                    }
                });
            });
        });
    }
}
