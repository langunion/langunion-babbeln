use crate::actors::{
    use_app_handle, use_blocked_users_handle, use_friends_handle, AppHandle, BlockedUsersHandle,
    FriendsHandle, HttpError, HttpHandle,
};
use crate::core::models::User;
use crate::core::responses::{BlockUserResponse, RemoveFriendResponse};
use crate::modals::ProfileModal;
use langunion_ui::{colors, Button, Container, ContainerComponent};
use egui::{vec2, Context, RichText, Stroke, Ui};
use tokio::sync::oneshot;

pub struct FriendItem {
    app_handle: AppHandle,
    friends_handle: FriendsHandle,
    blocked_users_handle: BlockedUsersHandle,
    user: User,
    unfriend_response: Option<oneshot::Receiver<Result<RemoveFriendResponse, HttpError>>>,
    block_response: Option<oneshot::Receiver<Result<BlockUserResponse, HttpError>>>,
    profile_modal: ProfileModal,
}

impl FriendItem {
    pub fn new(user: User) -> Self {
        Self {
            app_handle: use_app_handle(),
            friends_handle: use_friends_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            user: user.clone(),
            unfriend_response: None,
            block_response: None,
            profile_modal: ProfileModal::new(user),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.profile_modal.render(ui, ctx);

        let dark_mode = ui.style().visuals.dark_mode;

        Container::new()
            .padding(10.)
            .full_width(ui)
            .background(match dark_mode {
                false => *colors::STONE_50,
                true => *colors::ZINC_800,
            })
            .border(match dark_mode {
                false => Stroke::new(1., *colors::STONE_100),
                true => Stroke::new(1., *colors::ZINC_700),
            })
            .show(ui, |ui| {
                ui.horizontal(|ui: &mut Ui| {
                    Container::new()
                        .radius(5.)
                        .border(match dark_mode {
                            false => (3., *colors::GRAY_300).into(),
                            true => (3., *colors::GRAY_700).into(),
                        })
                        .background(match dark_mode {
                            false => *colors::GRAY_200,
                            true => *colors::GRAY_600,
                        })
                        .show(ui, |ui| {
                            ui.add(
                                egui::Image::from_uri(&self.user.avatar)
                                    .fit_to_exact_size(vec2(40., 40.))
                                    .rounding(5.),
                            );
                        });

                    ui.label(
                        RichText::new(&self.user.username)
                            .color(match dark_mode {
                                false => colors::WHITE,
                                true => colors::WHITE,
                            })
                            .size(14.),
                    );

                    if Button::new()
                        .label("Show Profile")
                        .normal(ui)
                        .add(ui)
                        .clicked()
                    {
                        self.show_profile();
                    }

                    if Button::new()
                        .label("Open chat")
                        .normal(ui)
                        .add(ui)
                        .clicked()
                    {
                        self.open_chat(self.user.id);
                    }

                    if Button::new().label("Block").normal(ui).add(ui).clicked() {
                        self.block(self.user.id);
                    }

                    if Button::new().label("Unfriend").normal(ui).add(ui).clicked() {
                        self.unfriend(self.user.id);
                    }
                });
            });
    }

    fn show_profile(&mut self) {
        self.profile_modal.open();
    }

    fn open_chat(&mut self, user_id: usize) {
        self.app_handle.set_show_chat(user_id);
    }

    fn block(&mut self, user_id: usize) {
        let receiver = BlockedUsersHandle::block_user(user_id);
        self.block_response = Some(receiver);
    }

    fn unfriend(&mut self, user_id: usize) {
        let receiver = FriendsHandle::unfriend_user(user_id);
        self.unfriend_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.unfriend_response,
            |_data: RemoveFriendResponse| {
                self.friends_handle.fetch();
            },
            |_error| {},
        );

        HttpHandle::handle_response_data(
            &mut self.block_response,
            |_data: BlockUserResponse| self.blocked_users_handle.fetch(),
            |_error| {},
        );
    }
}
