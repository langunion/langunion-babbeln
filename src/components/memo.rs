use crate::actors::{
    use_app_handle, use_messages_handle, use_user_handle, AppHandle, MessagesHandle,
};
use crate::core::models::User;
use langunion_ui::Button;
use egui::{Context, Sense, Ui};
use log::error;

pub struct Memo {
    app_handle: AppHandle,
    messages_handle: MessagesHandle,
    me: Option<User>,
}

impl Memo {
    pub fn new() -> Self {
        Self {
            app_handle: use_app_handle(),
            messages_handle: use_messages_handle(),
            me: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        let is_active: bool = {
            let chat_partner_id: usize = self.app_handle.show_chat().unwrap_or(0);
            match &self.me {
                Some(me) => me.id == chat_partner_id,
                None => false,
            }
        };

        let memo_button = Button::new()
            .label("Note to Self")
            .is_primary(is_active, ui)
            .full_width(true)
            .add(ui);

        memo_button.context_menu(|ui| {
            if ui.button("remove all messages").clicked() {
                self.handle_remove_clicked();
                ui.close_menu();
            }
        });

        if memo_button.clicked() {
            self.handle_memo_clicked();
        }
    }

    fn handle_memo_clicked(&mut self) {
        let me = use_user_handle().me();
        self.app_handle.set_show_chat(me.id);
        self.messages_handle.fetch_chat(me.id);
        self.me = Some(me);
    }

    fn handle_remove_clicked(&mut self) {
        error!("todo remove all messages button clicked");
    }
}
