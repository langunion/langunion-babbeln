use crate::actors::{
    use_app_handle, use_blocked_users_handle, use_chat_partners_handle, use_friends_handle,
    BlockedUsersHandle, ChatPartnersHandle, FriendsHandle,
};
use crate::components::{DirectMessageItem, Memo};
use crate::core::models::User;
use crate::core::task::{handle_oneshot_receiver, spawn_blocking};
use langunion_ui::{colors, Heading, Styles};
use egui::{Context, Ui};
use log::debug;
use tokio::sync::oneshot;

pub struct DirectMessages {
    chat_partners_handle: ChatPartnersHandle,
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    memo: Memo,
    direct_message_items: Vec<DirectMessageItem>,
    get_direct_message_items: Option<oneshot::Receiver<Vec<DirectMessageItem>>>,
}

impl DirectMessages {
    pub fn new() -> Self {
        Self {
            chat_partners_handle: use_chat_partners_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            friends_handle: use_friends_handle(),
            memo: Memo::new(),
            direct_message_items: Vec::new(),
            get_direct_message_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_direct_message_items();

        Heading::new("Direct Messages")
            .size(1)
            .color(match ui.style().visuals.dark_mode {
                false => Some(*colors::STONE_500),
                true => Some(*colors::ZINC_500),
            })
            .add(ui);

        egui::ScrollArea::vertical()
            .max_width(std::f32::INFINITY)
            .max_height(std::f32::INFINITY)
            .auto_shrink([false, false])
            .show_viewport(ui, |ui: &mut Ui, _| {
                self.memo.render(ui, ctx);

                self.direct_message_items
                    .iter_mut()
                    .for_each(|item: &mut DirectMessageItem| {
                        item.render(ui, ctx);
                    });
            });
    }

    fn handle_update_direct_message_items(&mut self) {
        if self
            .chat_partners_handle
            .chat_partners
            .has_changed()
            .unwrap()
            || self
                .blocked_users_handle
                .blocked_users
                .has_changed()
                .unwrap()
            || self.friends_handle.friends.has_changed().unwrap()
        {
            debug!("DirectMessages/chat partners changed");
            // IMPORTANT: get blocked users and friends to make it "seen"
            let _blocked_users = self.blocked_users_handle.blocked_users();
            let _friends = self.friends_handle.friends();
            self.build_direct_message_items();
        }

        handle_oneshot_receiver(
            &mut self.get_direct_message_items,
            |data: Vec<DirectMessageItem>| {
                debug!("Build DirectMessageItems finished");
                self.direct_message_items = data;
            },
        );
    }

    fn build_direct_message_items(&mut self) {
        debug!("Build DirectMessageItems");
        let (sender, receiver) = oneshot::channel::<Vec<DirectMessageItem>>();
        let chat_partners: Vec<User> = self.chat_partners_handle.get_chat_partners();
        spawn_blocking(|| {
            let direct_message_items: Vec<DirectMessageItem> = chat_partners
                .into_iter()
                .map(|user: User| DirectMessageItem::new(user))
                .collect();
            let _ = sender.send(direct_message_items);
            use_app_handle().request_repaint();
        });
        self.get_direct_message_items = Some(receiver);
    }
}
