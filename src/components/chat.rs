use crate::actors::{
    use_app_handle, use_blocked_users_handle, use_chat_partners_handle, use_friends_handle,
    use_http_handle, use_messages_handle, AppHandle, BlockedUsersHandle, FriendsHandle, HttpError,
    HttpHandle, MessagesHandle, UserHandle,
};
use crate::components::ChatMessages;
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::SendMessageResponse;
use langunion_ui::{colors, Button, ButtonImage, Container, ContainerComponent, Heading, Input};
use egui::{vec2, Context, Stroke, Ui};
use log::{debug, warn};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct Chat {
    chat_partner: Option<User>,
    app_handle: AppHandle,
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    messages_handle: MessagesHandle,
    chat_messages: ChatMessages,
    message: String,
    send_response: Option<oneshot::Receiver<Result<SendMessageResponse, HttpError>>>,
}

impl Chat {
    pub fn new() -> Self {
        Self {
            chat_partner: None,
            app_handle: use_app_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            friends_handle: use_friends_handle(),
            messages_handle: use_messages_handle(),
            chat_messages: ChatMessages::new(),
            message: String::new(),
            send_response: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_send();
        self.handle_chat_partner();

        let chat_partner = self
            .chat_partner
            .clone()
            .expect("'chat_partner' should exist on render");
        let chat_partner_username = UserHandle::display_username(&chat_partner);

        egui::TopBottomPanel::top("chat_top_panel")
            .exact_height(50.)
            .resizable(false)
            .show_separator_line(false)
            .show_inside(ui, |ui: &mut Ui| {
                egui::SidePanel::right("chat_top_panel_side_panel")
                    .show_separator_line(false)
                    .resizable(false)
                    .show_inside(ui, |ui: &mut Ui| {
                        ui.horizontal_centered(|ui| {
                            if Button::new()
                                .image(ButtonImage {
                                    id: "bytes://phone-call.svg".into(),
                                    bytes: include_bytes!("../../assets/icons/phone-call.svg"),
                                    size: vec2(15., 15.),
                                })
                                .tooltip("Start audio call")
                                .normal(ui)
                                .add(ui)
                                .clicked()
                            {
                                create_toast(NotificationKind::Warning, "Start audio call");
                                warn!("Start audio call");
                            }

                            if Button::new()
                                .image(ButtonImage {
                                    id: "bytes://video.svg".into(),
                                    bytes: include_bytes!("../../assets/icons/video.svg"),
                                    size: vec2(15., 15.),
                                })
                                .tooltip("Start video call")
                                .normal(ui)
                                .add(ui)
                                .clicked()
                            {
                                create_toast(NotificationKind::Warning, "Start video call");
                                warn!("Start video call");
                            }

                            if Button::new()
                                .image(ButtonImage {
                                    id: "bytes://search.svg".into(),
                                    bytes: include_bytes!("../../assets/icons/search.svg"),
                                    size: vec2(15., 15.),
                                })
                                .tooltip("Search messages")
                                .normal(ui)
                                .add(ui)
                                .clicked()
                            {
                                create_toast(NotificationKind::Warning, "Search messages");
                                warn!("Search messages");
                            }
                        });
                    });

                egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
                    ui.horizontal_centered(|ui: &mut Ui| {
                        Container::new()
                            .border(Stroke::new(2., *colors::EMERALD_300))
                            .show(ui, |ui| {
                                ui.add(
                                    egui::Image::from_uri(&chat_partner.avatar)
                                        .fit_to_exact_size(vec2(30., 30.)),
                                );
                            });

                        Heading::new(format!("@{chat_partner_username}"))
                            .size(3)
                            .add(ui);
                    });
                });
            });

        egui::TopBottomPanel::bottom("chat_bottom_panel")
            .exact_height(50.)
            .resizable(false)
            .show_separator_line(false)
            .show_inside(ui, |ui: &mut Ui| {
                egui::SidePanel::right("chat_bottom_panel_side_panel")
                    .exact_width(50.)
                    .show_separator_line(false)
                    .resizable(false)
                    .show_inside(ui, |ui: &mut Ui| {
                        ui.horizontal_centered(|ui| {
                            if Button::new()
                                .image(ButtonImage {
                                    id: "bytes://send.svg".into(),
                                    bytes: include_bytes!("../../assets/icons/send.svg"),
                                    size: vec2(15., 15.),
                                })
                                .tooltip("Send a message")
                                .normal(ui)
                                .add_enabled(
                                    self.send_response.is_none() && !self.message.is_empty(),
                                    ui,
                                )
                                .clicked()
                            {
                                self.send();
                            }
                        });
                    });

                egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
                    ui.horizontal_centered(|ui| {
                        Input::new(&mut self.message)
                            .normal(&ui)
                            .placeholder(format!("Message @{chat_partner_username}"))
                            .full_width()
                            .add(ui);
                    });
                });
            });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            Container::new()
                .radius(10.)
                .padding((3., 10.))
                .full_width(ui)
                .full_height(ui)
                .background(match ui.style().visuals.dark_mode {
                    false => *colors::STONE_50,
                    true => *colors::ZINC_800,
                })
                .border(match ui.style().visuals.dark_mode {
                    false => Stroke::new(1., *colors::STONE_100),
                    true => Stroke::new(1., *colors::ZINC_700),
                })
                .show(ui, |ui| {
                    self.chat_messages
                        .render(ui, ctx, chat_partner, &chat_partner_username);
                });
        });
    }

    fn handle_chat_partner(&mut self) {
        if self.app_handle.show_chat.has_changed().unwrap()
            || self
                .blocked_users_handle
                .blocked_users
                .has_changed()
                .unwrap()
            || self.friends_handle.friends.has_changed().unwrap()
        {
            debug!("Chat title updated");
            self.blocked_users_handle.blocked_users();
            self.friends_handle.friends();
            self.chat_partner = UserHandle::get_user_by_id(self.app_handle.show_chat().unwrap());
        }
    }

    fn send(&mut self) {
        let to_user: usize = self.app_handle.show_chat().unwrap();
        let content: String = self.message.clone();

        let receiver = use_http_handle().do_authenticated_request::<SendMessageResponse>(
            Method::POST,
            "/messages/",
            Some(json!({
                "to_user": to_user,
                "content": content,
            })),
        );

        self.send_response = Some(receiver);
    }

    fn handle_send(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.send_response,
            |_data: SendMessageResponse| {
                let chat_partner_id: usize = self.app_handle.show_chat().unwrap();
                self.messages_handle.fetch_chat(chat_partner_id);
                use_chat_partners_handle().set_latest_chat_partner(chat_partner_id);
                self.message = String::new();
            },
            |error| {
                debug!("hit");
                create_toast(NotificationKind::Error, error.message);
            },
        );
    }
}
