use crate::actors::{use_blocked_users_handle, BlockedUsersHandle, HttpError, HttpHandle};
use crate::core::models::User;
use crate::core::responses::UnblockUserResponse;
use crate::modals::ProfileModal;
use egui::{Context, Ui};
use tokio::sync::oneshot;

pub struct BlockedUserItem {
    blocked_users_handle: BlockedUsersHandle,
    blocked_user: User,
    unblock_response: Option<oneshot::Receiver<Result<UnblockUserResponse, HttpError>>>,
    profile_modal: ProfileModal,
}

impl BlockedUserItem {
    pub fn new(blocked_user: User) -> Self {
        Self {
            blocked_users_handle: use_blocked_users_handle(),
            blocked_user: blocked_user.clone(),
            unblock_response: None,
            profile_modal: ProfileModal::new(blocked_user),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.profile_modal.render(ui, ctx);

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.blocked_user.username);

                if ui.button("show profile").clicked() {
                    self.show_profile();
                }

                if ui.button("unblock").clicked() {
                    self.unblock(self.blocked_user.id);
                }
            });
        });
    }

    fn show_profile(&mut self) {
        self.profile_modal.open();
    }

    fn unblock(&mut self, user_id: usize) {
        let receiver = BlockedUsersHandle::unblock_user(user_id);
        self.unblock_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpHandle::handle_response_data(
            &mut self.unblock_response,
            |_data: UnblockUserResponse| {
                self.blocked_users_handle.fetch();
            },
            |_error| {},
        );
    }
}
