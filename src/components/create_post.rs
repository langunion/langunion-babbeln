use egui::{Context, Ui};
use std::cell::RefCell;
use std::rc::Rc;

pub struct CreatePost {
    show_create_post: Rc<RefCell<bool>>,
}

impl CreatePost {
    pub fn new(show_create_post: Rc<RefCell<bool>>) -> Self {
        Self { show_create_post }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        egui::TopBottomPanel::top("create_post_top_panel").show_inside(ui, |ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label("header");

                if ui.button("close").clicked() {
                    *self.show_create_post.borrow_mut() = false;
                }
            });
        });

        egui::TopBottomPanel::bottom("create_post_bottom_panel")
            .show_inside(ui, |ui: &mut Ui| ui.label("bottom"));

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            ui.label("central");
        });
    }
}
