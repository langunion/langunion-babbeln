use crate::actors::{
    use_app_handle, use_http_handle, use_socket_handle, use_user_handle, HttpHandle, Window,
};
use crate::core::task::spawn;
use egui::{Context, Ui};

pub struct Loading {}

impl Loading {
    pub fn new() -> Self {
        spawn(async move {
            // TODO When the app first starts wait till an internet connection is established
            // the user will be logged out due to a NetworkError
            // maybe on first start on the app wait till internet connection
            // is established

            let app_handle = use_app_handle();
            let mut http_handle = use_http_handle();

            let has_access: bool = http_handle.access_token().is_some();
            let has_refresh: bool = http_handle.refresh_token().is_some();

            match has_access && has_refresh {
                true => {
                    use_http_handle().maybe_reauthenticate().await;
                    HttpHandle::fetch_all();
                    use_user_handle().me_async().await;
                    use_socket_handle().connect();
                    app_handle.set_window(Window::Main);
                }
                false => app_handle.set_window(Window::Login),
            }
        });

        Self {}
    }

    pub fn render(&mut self, ctx: &Context) {
        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            ui.label("loading...");
        });
    }
}
