use crate::components::{Login, Register};
use langunion_ui::{Button, ButtonImage};
use egui::{vec2, Context, Theme, Ui};

#[derive(PartialEq)]
pub enum Show {
    Login,
    Register,
}

pub struct LoginRegister {
    login: Login,
    register: Register,
    show: Show,
}

impl LoginRegister {
    pub fn new() -> Self {
        Self {
            login: Login::new(),
            register: Register::new(),
            show: Show::Login,
        }
    }

    pub fn render(&mut self, ctx: &Context) {
        egui::TopBottomPanel::top("login_top_panel").show(ctx, |ui| {
            ui.horizontal(|ui| {
                if Button::new()
                    .label("Login")
                    .is_primary(self.show == Show::Login, &ui)
                    .add(ui)
                    .clicked()
                {
                    self.show = Show::Login;
                }
                if Button::new()
                    .label("Sign up")
                    .is_primary(self.show == Show::Register, &ui)
                    .add(ui)
                    .clicked()
                {
                    self.show = Show::Register;
                }

                let current_theme = ctx.theme();
                let theme_button = match current_theme {
                    Theme::Light => Button::new()
                        .label("Light")
                        .normal(&ui)
                        .image(ButtonImage {
                            id: "bytes://sun.svg".into(),
                            bytes: include_bytes!("../../assets/icons/sun.svg"),
                            size: vec2(15., 15.),
                        })
                        .add(ui),
                    Theme::Dark => Button::new()
                        .label("Dark")
                        .normal(&ui)
                        .image(ButtonImage {
                            id: "bytes://moon.svg".into(),
                            bytes: include_bytes!("../../assets/icons/moon.svg"),
                            size: vec2(15., 15.),
                        })
                        .add(ui),
                };

                if theme_button.clicked() {
                    let next_theme = match current_theme {
                        Theme::Light => Theme::Dark,
                        Theme::Dark => Theme::Light,
                    };
                    ctx.set_theme(next_theme);
                }
            });
        });

        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            ui.set_height(ui.available_height());
            match self.show {
                Show::Login => self.login.render(ui, ctx),
                Show::Register => self.register.render(ui, ctx),
            }
        });
    }
}
