use crate::actors::{use_app_handle, use_reset_handle, use_user_handle, AppHandle, UserHandle};
use crate::components::{Chat, Dashboard, DirectMessages, Friends, Timeline};
use crate::core::task::spawn;
use langunion_ui::{colors, Button, ButtonImage, Container, Heading};
use egui::{vec2, Align, Context, Layout, Stroke, Theme, Ui};
use egui_flex::{item, Flex, FlexJustify};
use tokio::sync::oneshot;

pub struct Main {
    app_handle: AppHandle,
    user_handle: UserHandle,
    dashboard: Dashboard,
    friends: Friends,
    timeline: Timeline,
    chat: Chat,
    direct_messages: DirectMessages,
    logout_receiver: Option<oneshot::Receiver<()>>,
}

impl Main {
    pub fn new() -> Self {
        Self {
            app_handle: use_app_handle(),
            user_handle: use_user_handle(),
            dashboard: Dashboard::new(),
            friends: Friends::new(),
            timeline: Timeline::new(),
            chat: Chat::new(),
            direct_messages: DirectMessages::new(),
            logout_receiver: None,
        }
    }

    pub fn render(&mut self, ctx: &Context) {
        self.handle_logout();
        let me = self.user_handle.me();

        egui::SidePanel::left("main_left_panel")
            .resizable(false)
            .show_separator_line(false)
            .show(ctx, |ui| {
                ui.add_space(5.);

                ui.vertical_centered(|ui| {
                    ui.horizontal(|ui| {
                        Container::new()
                            .border(Stroke::new(3., *colors::RED_500))
                            .radius(5.0)
                            .show(ui, |ui| {
                                ui.add(
                                    egui::Image::new(me.avatar)
                                        .fit_to_exact_size(vec2(40., 40.))
                                        .rounding(5.),
                                );
                            });
                        Heading::new(match me.username.len() {
                            1..=7 => me.username,
                            _ => format!("{}...", me.username.clone().split_off(5)),
                        })
                        .size(3)
                        .add(ui);

                        ui.add_space(20.);

                        if Button::new()
                            .image(ButtonImage {
                                id: "bytes://log-out.svg".into(),
                                bytes: include_bytes!("../../assets/icons/log-out.svg"),
                                size: vec2(15., 15.),
                            })
                            .normal(ui)
                            .tooltip("Logout")
                            .add(ui)
                            .clicked()
                        {
                            self.logout(ui);
                        }

                        if Button::new()
                            .image(ButtonImage {
                                id: "bytes://settings.svg".into(),
                                bytes: include_bytes!("../../assets/icons/settings.svg"),
                                size: vec2(15., 15.),
                            })
                            .normal(ui)
                            .tooltip("Open settings")
                            .add(ui)
                            .clicked()
                        {
                            let next_theme = match ctx.theme() {
                                Theme::Light => Theme::Dark,
                                Theme::Dark => Theme::Light,
                            };
                            ctx.set_theme(next_theme);
                        }
                    });
                });

                if Button::new()
                    .label("Dashboard")
                    .is_primary(self.app_handle.show_dashboard(), ui)
                    .image(ButtonImage {
                        id: "bytes://layers.svg".into(),
                        bytes: include_bytes!("../../assets/icons/layers.svg"),
                        size: vec2(15., 15.),
                    })
                    .full_width(true)
                    .add(ui)
                    .clicked()
                {
                    self.app_handle.set_show_dashboard();
                }
                if Button::new()
                    .label("Friends")
                    .is_primary(self.app_handle.show_friends(), ui)
                    .image(ButtonImage {
                        id: "bytes://users.svg".into(),
                        bytes: include_bytes!("../../assets/icons/users.svg"),
                        size: vec2(15., 15.),
                    })
                    .full_width(true)
                    .add(ui)
                    .clicked()
                {
                    self.app_handle.set_show_friends();
                }
                if Button::new()
                    .label("Timeline")
                    .is_primary(self.app_handle.show_timeline(), ui)
                    .image(ButtonImage {
                        id: "bytes://feather.svg".into(),
                        bytes: include_bytes!("../../assets/icons/feather.svg"),
                        size: vec2(15., 15.),
                    })
                    .full_width(true)
                    .add(ui)
                    .clicked()
                {
                    self.app_handle.set_show_timeline();
                }

                self.direct_messages.render(ui, ctx);
            });

        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            if self.app_handle.show_dashboard() {
                self.dashboard.render(ui, ctx);
            } else if self.app_handle.show_friends() {
                self.friends.render(ui, ctx);
            } else if self.app_handle.show_timeline() {
                self.timeline.render(ui, ctx);
            } else if self.app_handle.show_chat().is_some() {
                self.chat.render(ui, ctx);
            }
        });
    }

    fn logout(&mut self, ui: &mut Ui) {
        let (sender, receiver) = oneshot::channel::<()>();
        ui.memory_mut(|m| *m = Default::default());
        self.logout_receiver = Some(receiver);

        spawn(async move {
            use_reset_handle().reset();
            // TODO check if this is true
            // wait till show_chat is none otherwise web will break
            // because web is slow and js event loop will cause all
            // the components to recreate and if chat or any component
            // that expects me to be there will crash eventhough reset_all
            // is called before but reset_all is async and it doesn't guarantee
            let _ = use_app_handle()
                .show_chat
                .wait_for(|user_id| user_id.is_none())
                .await;
            let _ = sender.send(());
        });
    }

    fn handle_logout(&mut self) {
        // Cannot use task::handle_oneshot_receiver because of double mutable self reference
        if let Some(response) = self.logout_receiver.as_mut() {
            match response.try_recv() {
                Ok(_) => {
                    *self = Self::new();
                    use_app_handle().request_repaint();
                }
                Err(oneshot::error::TryRecvError::Closed) => self.logout_receiver = None,
                Err(oneshot::error::TryRecvError::Empty) => (),
            }
        }
    }
}
