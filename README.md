<img 
    src="https://gitlab.com/langunion/langunion-babbeln/-/raw/master/icon.jpeg?ref_type=heads"
    align="right"
    width="100"
    height="100" 
/>

# langunion-babbeln
> Where language and community meets.  
> Licensed under `GNU GPLv3 or later`

Babbeln is an open source (video) chat platform for language learners.  
It's one of many applications/services provided by [langunion](https://www.langunion.com).

<hr />
    <p align="center">
        <img src="https://raw.githubusercontent.com/jitsi/jitsi-meet/master/readme-img1.png" width="900" />
    </p>
<hr />

Amongst others here are the main features Babbeln offers:

* [x] Native desktop application (no Electron etc.)
* [ ] Mobile applications
* [ ] HD audio and video
* [ ] Content sharing
* [ ] Chat with private conversations
* [ ] Virtual backgrounds

And many more!

## Using Babbeln

Try it out with a guest account on one of those platforms:

- [babbeln web](www.babbeln.app)
- [babbeln desktop](www.langunion.com/babbeln)
- [babbeln mobile](www.langunion.com/babbeln#mobile)

## Running your own instance

Use these make commands to make your life easier:

|           |                                                     |
|-----------|-----------------------------------------------------|
| prepare   | Install all the necessary tools for running the app |
| start     | Compile debug version of the app for native         |
| start_web | Start trunk server for web version of the app       |
| build     | Build a release version of the app for native       |
| build_web | Build a release version of the app for web          |

## Security

www.langunion.com/secruity

## Contributing

If you are looking to contribute to langunion, first of all, thank you! Please
see our [developer guide](www.langunion.com/developer-guide).

<br />
<br />

<footer>
    <p align="center" style="font-size: smaller;">
        Built with ❤️ by the at the moment only me Marc.
    </p>
</footer>
