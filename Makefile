prepare:
	@pacman -S rustup
	@rustup override 1.82.0
	@rustup target add wasm32-unknown-unknown
	@cargo install trunk
start:
	@cargo update
	@cargo run
start_web:
	@cargo update
	@trunk serve
build:
	@cargo update
	@cargo run --release
build_web:
	@cargo update
	@trunk build --release
