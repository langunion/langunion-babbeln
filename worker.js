if ("function" === typeof importScripts) {
    // PROBLEM: in webworkers you dont have access to the DOM
    // so therefore i cant automagically link the wasm file
    // that is being compiled with a hash aka. random file name
    // and I want there to be a hash so the web clients always gets the
    // newest wasm file if there is an update

    /* var js = [...document.head.children]
     *     .filter((e) => e.nodeName === "LINK")
     *     .filter((l) => l.href.includes("babbeln") && l.href.includes("bg.wasm"))[0]?.href;
     * var wasm = [...document.head.children]
     *     .filter((e) => e.nodeName === "LINK")
     *     .filter((l) => l.href.includes("babbeln") && l.href.includes("bg.wasm"))[0]?.href;
     * console.log({ js, wasm });
     */

    // https://www.tweag.io/blog/2022-11-24-wasm-threads-and-messages/
    // Thank you Joe Neeman
    importScripts("./path/to/wasm_bindgen/module.js")
    self.onmessage = async event => {
        const { child_entry_point } = await wasm_bindgen(
            "./path/to/wasm_bindgen/module_bg.wasm",
            event.data[0]
        );
        child_entry_point();
    };
}
